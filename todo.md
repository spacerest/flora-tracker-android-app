/ll to 6 decimal places
//NEXT todo add text onscreen returning users location and changing with location


//todo
// select waypoint from another view and then show it on map
// navigate to existing waypoints -
// navigate to new waypoint
//figure out how to put border around view
//change theme color
//use image isntead of text title
//grey out impossible button
//don't let user click "add waypoint" until map is loaded (mb related to above)
//add select all/ deselct all button on browse
//auto generate date for each wp


#updated android studies to 2.0.3(?) & followed https://www.androidtutorialpoint.com/intermediate/android-map-app-showing-current-location-android/

1. get google play API key
2. set up permissions in AndroidManifest.xml to allow access to:
- ACCESS_NETWORK_STATE
- INTERNET
- ACCESS_COARSE_LOCATION
- ACCESS_FINE_LOCATION
- OpenGL ES V2 (Required for Google Maps V2)
3. checked that app's gradle/bundle file had correct dependencies
4. added ConnectionCallbacks, OnConnectionFailedListener, and LocationListener to implements portion of MapsActivity
5. in onMapReady of MapsActivity, check whether permission is granted to access fine location. Set map type to hybrid(?)
6. made buildGoogleApiClient() function to do the above
7. define onConnected of MapsActivity & set intervals of 1000 ms for each new location request (come back to this to make sure battery life isn't drained w/o cause)
8. request permissions if none (come back to this to understand it more)
9. fixed import android.location.LocationListener; to import com.google.android.gms.location.LocationListener;
10. added manager activity and home page with button to link to map
11. added a temp background image in styles.xml
12. added a dialog builder and a dialog when asking permission from user whose devices requires an explanation(?)
13. added default lat/long(Chicago) and camera zoom level to activity_maps.xml file
14. added a longclicklistener in onMapReady of MapActivites, so that we can later use callback onMapLongClick to set a new marker at clicked latlng (// mark new waypoint)
15. added icon for onMapLongClick marker (res/drawable marker.png)
16. test (make sure android studios still works on other comp)
17. added some waypoint functionality buttons to map activity
18. updated "add waypoint" to link to one of those buttons (instead of onlongclicklistener) for adding waypoint to a current location
19. updated add waypoint dialog to update marker with title/description and show long/lat in dialog
20. updated silly marker image to have no padding, duh
21. installing yeoman on mac (need to install on other comp too) for mocking json with https://github.com/micromata/generator-http-fake-backend
22. gson test works (thanks to https://stackoverflow.com/questions/7467568/parsing-json-from-url and https://stackoverflow.com/questions/33621808/why-does-gson-fromjson-throw-a-jsonsyntaxexception-expected-some-type-but-was-s)
23. todo: working out how to display the waypoints from json
24. can get waypoints from json, todo: setup json-fake-backend on vps so testable from android
25. setup http-fake-backend on vps, currently debugging why InputStreamReader has problem
26. setup basic list view for browse waypoints following http://abhiandroid.com/ui/listview. Still need to fix json stuff, can read json from preexisting string but not from url : (

#TODO based on the above (loose threads)
1. Activities should strongly consider removing all location request when entering the background (for example at onPause()), or at least swap the request to a larger interval and lower quality.
3. revisit onRequestPermissionsResult and checkLocationPermission of MapActivity to understand how/what is necessary
4. revisit permissions request explanation in MapActivity (why exactly is this necessary, what should it say, does the user need to click thru it?)
5. do recycle view for list view of waypoints 
6. figure out why location sometimes changes a bit upon map load
7. set up OnDestroys and stuff


#General questions
- when to delete branches that have been merged?

#app functionality questions
- do ppl use this just to save waypoints and/or to revisit old waypoints?
- can it be useful to have several waypoints displayed at once?
- do ppl like to know when waypoints were created / and or "last updated/last visited?"
- when should you start new intents vs revisit existing intents?-

to do:
- display title / long/lat in top bar for selected waypoint in map activity
- make it look better (icons and titles and stuff)
  - white text
- home => splash screen
  - show last marked waypoint when opening
- error message if can't get location
- fix sensor (super shaky)
- why does "center location" not work in map view
- on marker click add options for editing
- figure out why blank waypoint list on first load after turning on phone

done:
- show distance to selected waypoint (length of line)
- add waypoint on long click
- draw line to selected waypoint from user's location
- satellite view
- let user select color for each waypoint
- prevent multiple activities from bottom bar
- on new waypoint draw path from current location already
- fix something weird going on with browse waypoints list view adaptor
- on marker click add options for deleting


- 

stuff to think about:
- have draggable waypoint icon when setting a waypoint? or "add waypoint here" default is ok?
- check how accurate is (in meters)
  - horizontal accuracy
  - vertical accuracy one day

)
