package com.example.spacerest.floratracker.classes;

import com.example.spacerest.floratracker.classes.Waypoint;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by SMP on 1/10/18.
 */

public class Waypoints {
    public String title;
    public String notes;
    public List<String> errors = new ArrayList<String>();
    public int total;
    public int limit;
    public List<Waypoint> pointsInfo = new ArrayList<Waypoint>();
    public ArrayList<Waypoint> points = new ArrayList<Waypoint>();

    public void addWaypoint(Double wpLat, Double wpLng, String wpTitle, String wpNotes, int wpColorInt) {
        Waypoint wp = new Waypoint(wpLat, wpLng, wpTitle, wpNotes, wpColorInt);
        wp.id = this.generateId();
        points.add(wp);
        return;
    }

    private String generateId() {
        //https://stackoverflow.com/questions/1389736/how-do-i-create-a-unique-id-in-java
        String newIdString = UUID.randomUUID().toString();
        return newIdString;
    }

    public Waypoint findWaypoint(String id){
        Waypoint foundWp = null;

        for (Waypoint wp : this.points) {
            if (id.equals(wp.id)) {
                foundWp = wp;
            }
        }

        return foundWp;
    }

    public void deleteWaypoint(Waypoint waypoint) {
        points.remove(waypoint);
    }

    int getPointsLength() {
        return points.size();
    }

    public void updateWaypoint(String id, Double wpLat, Double wpLng, String wpTitle, String wpNotes) {
        Waypoint waypoint = this.findWaypoint(id);
        waypoint.title = wpTitle;
        waypoint.notes = wpNotes;
        waypoint.lat = wpLat;
        waypoint.lng = wpLng;
    }
}
