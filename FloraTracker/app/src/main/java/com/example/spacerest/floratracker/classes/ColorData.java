package com.example.spacerest.floratracker.classes;

/**
 * Created by SMP on 4/21/18.
 */

public class ColorData {
    Integer imageId;
    public ColorData(Integer imageId){
        this.imageId = imageId;
    }

    public Integer getImageId() {
        return imageId;
    }

}
