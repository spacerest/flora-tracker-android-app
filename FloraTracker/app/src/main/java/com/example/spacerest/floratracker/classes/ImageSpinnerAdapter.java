package com.example.spacerest.floratracker.classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.utils.CustomUtils;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by SMP on 4/21/18.
 */

public class ImageSpinnerAdapter extends ArrayAdapter<ColorData> {

    int groupid;
    ArrayList<ColorData> list;
    LayoutInflater inflater;
    Context context;
    CustomUtils utils;
    int[] colorArray;
    public ImageSpinnerAdapter(Context context, int groupid, int id, ArrayList<ColorData>
            list){
        super(context, id,list);
        this.list=list;
        inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupid=groupid;
        this.context = context;
        this.utils = new CustomUtils();
        colorArray = context.getResources().getIntArray(R.array.color_int_array);

    }

    public View getView(int position, View convertView, ViewGroup parent ){
        View itemView=inflater.inflate(groupid,parent,false);
        ImageView imageView=(ImageView)itemView.findViewById(R.id.spinnerImage);
        //imageView.setImageResource(list.get(position).getImageId());
        Bitmap tintedBitmap = utils.getBitmapFromColor(context, colorArray[list.get(position).getImageId()]);
        imageView.setImageBitmap(tintedBitmap);
        //TextView textView=(TextView)itemView.findViewById(R.id.spinnerText);
        //textView.setText(list.get(position).getText());
        return itemView;
    }


    public View getDropDownView(int position, View convertView, ViewGroup
            parent){
        return getView(position,convertView,parent);

    }


}
