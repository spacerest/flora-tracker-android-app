package com.example.spacerest.floratracker.CustomClasses;

/**
 * Created by SMP on 5/21/18.
 */

import com.example.spacerest.floratracker.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Waypoint implements Serializable {

    public
        //class constructor
    Waypoint(Double wpLat, Double wpLng, String wpTitle, String wpNotes, int wpColor){
        lat = wpLat;
        lng = wpLng;
        title = wpTitle;
        notes = wpNotes;
        colorInt = wpColor;

        //set selected to false -- used in browse view for list adaptor
        selected = true;
        this.setMarkerOptions();
        Date currentDate = new Date();
        SimpleDateFormat smpleDateFormat = new SimpleDateFormat("yy-MM-dd HH:mm");//.getDateTimeInstance();
        dateAdded = smpleDateFormat.format(currentDate);

    }
    public String title;
    public String notes;
    public String dateAdded;
    public Double lat;
    public Double lng;
    public String id;
    public int colorInt;
    public int intId;
    public boolean selected;
    private transient MarkerOptions markerOptions;

    public void setMarkerOptions()
    {
        markerOptions = new MarkerOptions();
        LatLng latlng = new LatLng(lat, lng);
        markerOptions.position(latlng);
        markerOptions.snippet(notes);
        markerOptions.title(title);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.sprout_marker));
        markerOptions.isDraggable();
    }

}

