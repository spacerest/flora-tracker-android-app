package com.example.spacerest.floratracker.activities;
import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;

import com.example.spacerest.floratracker.classes.ColorData;
import com.example.spacerest.floratracker.classes.ImageSpinnerAdapter;
import com.example.spacerest.floratracker.classes.MapStateManager;
import com.example.spacerest.floratracker.utils.GetJsonIntentService;
import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.classes.Waypoint;
import com.example.spacerest.floratracker.classes.Waypoints;
import com.example.spacerest.floratracker.utils.CustomUtils;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
//https://stackoverflow.com/questions/42890669/cannot-resolve-symbol-locationservices#42890743
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import java.io.File;
import java.io.FileInputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

//How to use AppCombatActivity instead of FragmentActivity
//https://stackoverflow.com/questions/34072235/how-to-include-fragmentactivity-layout-in-activity-layout
//https://stackoverflow.com/questions/34072235/how-to-include-fragmentactivity-layout-in-activity-layout

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener {

  //util class to run custom utils (is this an ok way to do this?)
  CustomUtils utils;

  //variables: map
  private GoogleMap mMap;
  GoogleApiClient mGoogleApiClient;
  LocationRequest mLocationRequest;
  Location mLastLocation;
  boolean mapIsConnected = false;

  //variables: draw & keep track of markers
  Marker mCurrLocationMarker;
  Marker selectedMarker;
  String selectedMarkerId;
  MarkerOptions selectedMarkerOptions = new MarkerOptions();
  private HashMap<String, Marker> markerHashMap = new HashMap<String, Marker>();
  int[] colorArray;

  //variables: keep track of waypointManager
  ArrayList<Waypoint> wpList = new ArrayList<Waypoint>();
  ArrayList<Boolean> wpListSelected = new ArrayList<Boolean>();

  //variables: keep track of orientation
  SensorEventListener sensorEventListener;
  Float bearing;
  CameraPosition camPosition;
  private Sensor sensor;
  private Sensor sensorMag;
  private Sensor sensorAcc;
  private static SensorManager sensorManager;
  MapStateManager mapStateManager;
  int sensorSpeed = SensorManager.SENSOR_DELAY_UI;
  double previousOrientation = 0.0;
  private final float[] mAccelerometerReading = new float[3];
  private final float[] mMagnetometerReading = new float[3];
  private final float[] mRotationMatrix = new float[9];
  private final float[] mOrientationAngles = new float[3];

  //variables: draw path
  Polyline currentPath = null;
  Polyline currentPathBackground = null;

  //variables: pull down json (to be used later)
  private static final int JSON_DOWNLOAD_REQUEST_CODE = 0;
  public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

  /* -------------------------------------------------------
   *
   *  Life cycle functions - start
   *
   ---------------------------------------------------------*/
  @Override
  protected void onPause() {
    super.onPause();
    sensorManager.unregisterListener(mySensorEventListener, sensor);

  }

  @Override
  protected void onResume() {
    super.onResume();

    sensorManager.registerListener(mySensorEventListener, sensorMag, sensorSpeed);
    sensorManager.registerListener(mySensorEventListener, sensorAcc,
            sensorSpeed);
  }

  //open up activity when it's already been created
  //gets "checked waypointManager" from browse waypoint activity
  @Override
  protected void onNewIntent(Intent intent) {
    super.onNewIntent(intent);
    try {
      wpList = (ArrayList<Waypoint>) intent.getExtras().getSerializable("waypointManager");
      wpListSelected = (ArrayList<Boolean>) intent.getExtras().getSerializable("selected");

      int itemCount = wpList.size();

      Iterator it = markerHashMap.entrySet().iterator();

      mMap.clear();

      markerHashMap.clear();

      for(int i=itemCount-1; i >= 0; i--){
        if (wpListSelected.get(i)) {
          Marker newMarker = mMap.addMarker(setMarkerOptions(wpList.get(i)));
          markerHashMap.put(wpList.get(i).id, newMarker);
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);
    utils = new CustomUtils();

    //get possible colors from int array file
    colorArray = getResources().getIntArray(R.array.color_int_array);
    //start sensor stuff
    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    sensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    sensorMag = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    sensorAcc = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    mapStateManager = new MapStateManager(this);

    //check that i have permission to access device location
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      checkLocationPermission();
    }

    // Obtain the SupportMapFragment and get notified when the map is ready to be used.
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    //if activity is being opened for the first time from browse waypointManager, check what waypointManager to put on map
    try {
      wpList = (ArrayList<Waypoint>) getIntent().getExtras().getSerializable("waypointManager");
      wpListSelected = (ArrayList<Boolean>) getIntent().getExtras().getSerializable("selected");

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /* -------------------------------------------------------
   *
   *  Life cycle functions - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Google map functions - start
   *
   ---------------------------------------------------------*/
  // manipulates the map once available, triggered when map is ready to be used
  @Override
  public void onMapReady(GoogleMap googleMap) {
    //Toast.makeText(this, "getting map ready", Toast.LENGTH_LONG).show();
    mMap = googleMap;
    mMap.setMapType(mMap.MAP_TYPE_SATELLITE);
    mMap.setOnMapLongClickListener(this);

    //Initialize Google Play Services
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      if (ContextCompat.checkSelfPermission(this,
              Manifest.permission.ACCESS_FINE_LOCATION)
              == PackageManager.PERMISSION_GRANTED) {
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);
          /*if (sensor!=null) {
              sensorManager.registerListener(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_UI);
              Toast.makeText(this, "hi", Toast.LENGTH_LONG);
          }*/
      }
    }
    else {
      buildGoogleApiClient();
      mMap.setMyLocationEnabled(true);
        /*if (sensor!=null) {
            sensorManager.registerListener(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_UI);
            Toast.makeText(this, "hi", Toast.LENGTH_LONG);
        }*/
    }
    //sensorManager.registerListener(mySensorEventListener, sensor, sensorSpeed);
  }

  protected synchronized void buildGoogleApiClient() {
    //https://stackoverflow.com/questions/37102240/cannot-resolve-symbol-mgoogleapiclient-android-studio
    mGoogleApiClient = new GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
    mGoogleApiClient.connect();
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {
    //Toast.makeText(this, "map is connected", Toast.LENGTH_LONG).show();

    mLocationRequest = new LocationRequest(); //make a new location request for this connection
    mLocationRequest.setInterval(1000); //checks for location update every 1000 ms
    mLocationRequest.setFastestInterval(1000); // Do be careful to also throttle setFastestInterval(long) if you perform heavy-weight work after receiving an update - such as using the network.
    mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    mLocationRequest.setSmallestDisplacement(5); //10 meters
    if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
      LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }
    mMap.setOnMarkerClickListener((GoogleMap.OnMarkerClickListener) this);

    int itemCount = wpList.size();

    for(int i=itemCount-1; i >= 0; i--){
      if (wpListSelected.get(i)) {
        Marker newMarker = mMap.addMarker(setMarkerOptions(wpList.get(i)));
        markerHashMap.put(wpList.get(i).id, newMarker);
      }
    }
    //sensorManager.registerListener(mySensorEventListener, sensor, sensorSpeed);

    mapIsConnected = true;

  }

  @Override
  public void onLocationChanged(Location location) {
    mLastLocation = location;
    //if there's already a marker, plz delete it to give the illusion of animated movement
    if (mCurrLocationMarker != null) {
      mCurrLocationMarker.remove();
    }

    if (currentPath != null && selectedMarker != null) {
      drawNewPath(selectedMarker, mLastLocation);
    }

    //move map camera to follow new location
    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
    updateCamera(location.getBearing());

    /*stop location updates
    if (mGoogleApiClient != null) {
      LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }*/
  }

  public LatLng getLocation(Location location) {
    mLastLocation = location;
    LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
    return latlng;
  }

  @Override
  public void onConnectionSuspended(int i) {
    Toast.makeText(this, "suspended", Toast.LENGTH_LONG);
      /*if (sensor!=null) {
          sensorManager.unregisterListener(mySensorEventListener);
          //sensorManager.remove(mySensorEventListener, sensor, SensorManager.SENSOR_DELAY_UI);
          Toast.makeText(this, "hi", Toast.LENGTH_LONG);
      }*/
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

  }

  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String permissions[], int[] grantResults) {
    switch (requestCode) {
      case MY_PERMISSIONS_REQUEST_LOCATION: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

          // Permission was granted.
          if (ContextCompat.checkSelfPermission(this,
                  Manifest.permission.ACCESS_FINE_LOCATION)
                  == PackageManager.PERMISSION_GRANTED) {
            if (mGoogleApiClient == null) {
              buildGoogleApiClient();
            }
            mMap.setMyLocationEnabled(true);
          }
        } else {
          // Permission denied, Disable the functionality that depends on this permission.
          Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
        }
        return;
      }
      // other 'case' lines to check for other permissions this app might request.
      //You can add here other case statements according to your requirement.
    }
  }

  public boolean checkLocationPermission(){
    if (ContextCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
      // Asking user if explanation is needed
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
              Manifest.permission.ACCESS_FINE_LOCATION)) {

        // Show an expanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Location permissions request");
        builder.setMessage("Location permission is needed to show an accurate map.");
        AlertDialog dialog = builder.create();
        dialog.show();

        //Prompt the user once explanation has been shown
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
      } else {
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
      }
      return false;
    } else {
      return true;
    }
  }

  /* -------------------------------------------------------
   *
   *  Google map functions - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Json web functions - start
   *  (currently not useful - to maybe use later)
   *
   ---------------------------------------------------------*/

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == JSON_DOWNLOAD_REQUEST_CODE) {
      switch (resultCode) {
        case GetJsonIntentService.INVALID_URL_CODE:
          handleInvalidURL();
          break;
        case GetJsonIntentService.ERROR_CODE:
          handleError(data);
          break;
        case 5:
          testIntentHandling(data);
          //handleJSON(data);
          break;

      }
      handleJSON(data);
    }
    super.onActivityResult(requestCode, resultCode, data);
  }

  private void testIntentHandling(Intent data) {
    String returnValue = data.getStringExtra("some_key");
  }

  private void handleJSON(Intent data) {
    Uri uri = data.getData();
    //data.getBundleExtra(GetJsonIntentService.JSON_RESULT_EXTRA);//.get(GetJsonIntentService.JSON_RESULT_EXTRA);
  }

  private void handleError(Intent data) {
    Toast.makeText(MapsActivity.this, "in handleERROR", Toast.LENGTH_LONG).show();
  }

  private void handleInvalidURL() {
    Toast.makeText(MapsActivity.this, "in handleInvalidURL", Toast.LENGTH_LONG).show();
  }


  /* -------------------------------------------------------
   *
   *  Json web functions - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  User nav action functions - start
   *
   ---------------------------------------------------------*/

  public void openBrowse(View view) {
    Intent intent = new Intent(MapsActivity.this, BrowseWaypointsActivity.class);
    Bundle bundle = new Bundle();
    bundle.putSerializable("waypointManager", wpList);
    bundle.putSerializable("selected", wpListSelected);
    intent.putExtras(bundle);
    startActivity(intent);
  }

  public void openHome(View view) {
    Intent intent = new Intent(this, Manager.class);
    startActivity(intent);
  }

  public void userDeleteWaypoint(View view){

    if (selectedMarker != null) {
      final Marker markerToRemove = markerHashMap.get(selectedMarkerId);

      AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

      alertDialogBuilder.setTitle("Deleting waypoint").setMessage("This will delete waypoint from your list of waypointManager permanently")
              .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int id) {
                  deleteWaypoint(markerToRemove);
                  removeMapMarker(markerToRemove);
                }}
              ).setNeutralButton("Cancel", null);
      AlertDialog dialog = alertDialogBuilder.create();
      dialog.show();
    }
  }

  public void addWaypointHere(View view) throws Exception {

    //https://stackoverflow.com/a/5763969/5650506
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    LayoutInflater inflater = getLayoutInflater();
    final View dialogView = inflater.inflate(R.layout.waypoint_details_dialog, null);

    // set prompts.xml to alertdialog builder
    alertDialogBuilder.setView(dialogView);

    //alertDialogBuilder.setTitle(R.string.add_waypoint_here);

    ArrayList<ColorData> colorDataList = new ArrayList<ColorData>();

    for (int j = 0; j < colorArray.length; j++) {
      colorDataList.add(new ColorData(j));
    }

    Spinner spinner = (Spinner) dialogView.findViewById(R.id.color_spinner);

    ImageSpinnerAdapter spinnerAdapter = new ImageSpinnerAdapter(this, R.layout.image_spinner_row,
    R.id.spinnerImage, colorDataList);

    /*ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
            R.array.color_array
            , android.R.layout.simple_spinner_item);*/
    //spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    spinner.setAdapter(spinnerAdapter);

    EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);
    EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);

    final LatLng latlng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

    final String fileName = getResources().getString(R.string.json_file_name);
    final String filePath = this.getFilesDir() + "/" + fileName;


    String lat = String.valueOf(latlng.latitude);
    String lng = String.valueOf(latlng.longitude);
    etLong.setText(lng, TextView.BufferType.EDITABLE);
    etLat.setText(lat, TextView.BufferType.EDITABLE);

    // set dialog message
    AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        EditText etTitle = (EditText) dialogView.findViewById(R.id.waypoint_detail_title_id);
        EditText etNotes = (EditText) dialogView.findViewById(R.id.waypoint_detail_notes_id);
        EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);
        EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);
        Spinner colorSpinner = (Spinner) dialogView.findViewById(R.id.color_spinner);

        String wpDetailsTitle = etTitle.getText().toString();
        String wpDetailsNotes = etNotes.getText().toString();
        Double wpDetailsLat = Double.parseDouble(etLat.getText().toString());
        Double wpDetailsLong = Double.parseDouble(etLong.getText().toString());
        int wpColorInt = colorArray[(int) colorSpinner.getSelectedItemId()];

        Waypoint waypoint = new Waypoint(wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);

        try {

          Waypoints allWaypoints = utils.getCurrentWaypoints(fileName, MapsActivity.this);

          //add new waypoint to existing waypointManager
          int wpCount = allWaypoints.points.size();
          allWaypoints.addWaypoint(wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);

          utils.writeUpdatedJson(fileName, allWaypoints, MapsActivity.this);

          Marker newMarker = mMap.addMarker(setMarkerOptions(allWaypoints.points.get(wpCount)));
          markerHashMap.put(allWaypoints.points.get(wpCount).id, newMarker);
          wpList.add(allWaypoints.points.get(wpCount));
          wpListSelected.add(true);

          //put sprout icon at new place
          if ( selectedMarker!= null) {
            selectedMarker.remove();
          }

          LatLng pos = new LatLng( wpDetailsLat, wpDetailsLong);
          selectedMarkerOptions.position(pos);
          selectedMarkerOptions.icon(getSelectedMarkerFromDrawable());
          selectedMarker = mMap.addMarker(selectedMarkerOptions);
          selectedMarkerId = allWaypoints.points.get(wpCount).id;
          //markerList.add(selectedMarker);
          selectedMarker.showInfoWindow();
        } catch (Exception e) {
          e.printStackTrace();
        }

      }
    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        Toast.makeText(MapsActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
      }
    });

    // create alert dialog
    AlertDialog alertDialog = alertDialogBuilder.create();
    // show it
    alertDialog.show();
  }

  /* -------------------------------------------------------
   *
   *  User nav action functions - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  User other functions - start
   *
   ---------------------------------------------------------*/

  @Override
  public void onMapLongClick(LatLng latlng) {

    //https://stackoverflow.com/a/5763969/5650506
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    LayoutInflater inflater = getLayoutInflater();
    final View dialogView = inflater.inflate(R.layout.waypoint_details_dialog, null);

    ArrayList<ColorData> colorDataList = new ArrayList<ColorData>();

    for (int j = 0; j < colorArray.length; j++) {
      colorDataList.add(new ColorData(j));
    }

    // set prompts.xml to alertdialog builder
    alertDialogBuilder.setView(dialogView);

    DecimalFormat df = new DecimalFormat("#.#####");
    alertDialogBuilder.setTitle(R.string.add_waypoint_here);
    EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);
    EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);

    //final LatLng latlng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

    final String fileName = getResources().getString(R.string.json_file_name);
    final String filePath = this.getFilesDir() + "/" + fileName;

    String lat = df.format(latlng.latitude);
    String lng = df.format(latlng.longitude);
    etLong.setText(lng, TextView.BufferType.EDITABLE);
    etLat.setText(lat, TextView.BufferType.EDITABLE);
    final Spinner colorSpinner = (Spinner) dialogView.findViewById(R.id.color_spinner);

    ImageSpinnerAdapter spinnerAdapter = new ImageSpinnerAdapter(this, R.layout.image_spinner_row,
            R.id.spinnerImage, colorDataList);

    colorSpinner.setAdapter(spinnerAdapter);


    // set dialog message
    AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        EditText etTitle = (EditText) dialogView.findViewById(R.id.waypoint_detail_title_id);
        EditText etNotes = (EditText) dialogView.findViewById(R.id.waypoint_detail_notes_id);
        EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);
        EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);

        String wpDetailsTitle = etTitle.getText().toString();
        String wpDetailsNotes = etNotes.getText().toString();
        Double wpDetailsLat = Double.parseDouble(etLat.getText().toString());
        Double wpDetailsLong = Double.parseDouble(etLong.getText().toString());
        int wpColorInt = colorArray[(int) colorSpinner.getSelectedItemId()];


        Waypoint waypoint = new Waypoint(wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);

        FileInputStream is = null;
        File jsonFile = new File(filePath);

        try {
          Waypoints allWaypoints = utils.getCurrentWaypoints(fileName, MapsActivity.this);

          //add new waypoint to existing waypointManager
          int wpCount = allWaypoints.points.size();
          allWaypoints.addWaypoint(wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);
          utils.writeUpdatedJson(fileName, allWaypoints, MapsActivity.this);
          Marker newMarker = mMap.addMarker(setMarkerOptions(allWaypoints.points.get(wpCount)));
          markerHashMap.put(allWaypoints.points.get(wpCount).id, newMarker);

          wpList.add(allWaypoints.points.get(wpCount));
          wpListSelected.add(true);

          //put sprout icon at new place
          if ( selectedMarker!= null) {
            selectedMarker.remove();
          }
          LatLng pos = new LatLng( wpDetailsLat, wpDetailsLong);
          selectedMarkerOptions.position(pos);
          selectedMarkerOptions.icon(getSelectedMarkerFromDrawable());
          selectedMarker = mMap.addMarker(selectedMarkerOptions);
          selectedMarkerId = allWaypoints.points.get(wpCount).id;
          selectedMarker.showInfoWindow();
        } catch (Exception e) {
          e.printStackTrace();
        }

      }
    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        Toast.makeText(MapsActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
      }
    });

    // create alert dialog
    AlertDialog alertDialog = alertDialogBuilder.create();
    // show it
    alertDialog.show();
  }
    /* -------------------------------------------------------
   *
   *  User other functions - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Marker functions - start
   *
   ---------------------------------------------------------*/

  @Override
  public boolean onMarkerClick(Marker marker) {

    Iterator it = markerHashMap.entrySet().iterator();
    if ( selectedMarker!= null) {
      selectedMarker.remove();
    }

    LatLng pos = marker.getPosition();
    selectedMarkerOptions.position(pos);
    selectedMarkerOptions.icon(getSelectedMarkerFromDrawable());
    marker.hideInfoWindow();
    selectedMarker = mMap.addMarker(selectedMarkerOptions);
    selectedMarker.showInfoWindow();

    while (it.hasNext()) {
      HashMap.Entry pair = (HashMap.Entry) it.next();
      Marker m = (Marker) pair.getValue();
      if (marker.equals(m)) {
        selectedMarkerId = (String) pair.getKey();
        break;
      }
    }

    drawNewPath(marker, mLastLocation);

    return false;
  }

  public void deleteWaypoint(Marker markerToRemove) {

    final String fileName = getResources().getString(R.string.json_file_name);
    final String filePath = this.getFilesDir() + "/" + fileName;
    Waypoints wps;

    wps = utils.getCurrentWaypoints(fileName, this);

    //remove waypoint from existing waypointManager
    for (Waypoint wp : wps.points) {
      if (wp.id.equals(selectedMarkerId)) {
        wps.deleteWaypoint(wp);
        //wpListSelected.remove(wpList.indexOf(wp));
        //wpList.remove(wp);
      }
    }

    Waypoint wpToRemove = null;
    for (Waypoint wp : wpList) {
      if (wp.id.equals(selectedMarkerId)) {
        wpToRemove = wp;
        wpListSelected.remove(wpList.indexOf(wp));
      }
    }
    if (wpToRemove != null) { wpList.remove(wpToRemove); };

    utils.writeUpdatedJson(fileName, wps,this);
  }

  public void removeMapMarker(Marker markerToRemove) {
    selectedMarker.remove();
    markerToRemove.remove();
    markerHashMap.remove(selectedMarkerId);
    selectedMarkerId = null;
    currentPath.remove();
    currentPathBackground.remove();
    selectedMarker = null;
  }

  private MarkerOptions setMarkerOptions(Waypoint wp) {
    MarkerOptions markerOptions = new MarkerOptions();
    LatLng latlng = new LatLng(wp.lat, wp.lng);
    markerOptions.position(latlng);
    //markerOptions.snippet(wp.notes);
    markerOptions.title(wp.title);

    markerOptions.icon(getMarkerIconFromDrawable(wp.colorInt));
    markerOptions.anchor(0.5f, 0.5f);
    markerOptions.isDraggable();
    return markerOptions;
  }

  private BitmapDescriptor getSelectedMarkerFromDrawable() {
    Drawable sproutD = getResources().getDrawable(R.drawable.sprout_marker);
    Bitmap sourceBitmap = utils.convertDrawableToBitmap(sproutD);
    return BitmapDescriptorFactory.fromBitmap(sourceBitmap);
  }

  private BitmapDescriptor getMarkerIconFromDrawable(int colorChosen) {

    Drawable d = getResources().getDrawable(R.drawable.circle_marker);
    Drawable d2 = getResources().getDrawable(R.drawable.circle_outline);

    //Convert drawable in to bitmap
    d.setColorFilter(colorChosen, PorterDuff.Mode.SRC_ATOP);
    LayerDrawable ld = new LayerDrawable(new Drawable[] {d, d2});

    Bitmap sourceBitmap = utils.convertDrawableToBitmap(ld);


    return BitmapDescriptorFactory.fromBitmap(sourceBitmap);
  }

  /* -------------------------------------------------------
   *
   *  Markers - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Sensors listeners and functions - start
   *
   ---------------------------------------------------------*/

  SensorEventListener mySensorEventListener = new SensorEventListener() {
    @Override
    public void onSensorChanged(SensorEvent event) {
      //mLastLocation.setBearing(event.values[0]);

      if (mMap != null && mapIsConnected) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
          System.arraycopy(event.values, 0, mAccelerometerReading,
                  0, mAccelerometerReading.length);
          updateOrientationAngles();
        }
        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
          System.arraycopy(event.values, 0, mMagnetometerReading,
                  0, mMagnetometerReading.length);
          updateOrientationAngles();
        }

        if (Math.abs(Math.toDegrees(mOrientationAngles[0]) - Math.toDegrees(previousOrientation)) > 1.0) {
          float bearing = (float) Math.toDegrees(mOrientationAngles[0]);// + mDeclination;
          previousOrientation = mOrientationAngles[0];
          updateCamera(bearing);
        }
      }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
      //Toast.makeText(MapsActivity.this, "sensor acc changed", Toast.LENGTH_LONG).show();
    }
  };

  public void updateOrientationAngles() {
    // Update rotation matrix, which is needed to update orientation angles.
    sensorManager.getRotationMatrix(mRotationMatrix, null,
            mAccelerometerReading, mMagnetometerReading);

    // "mRotationMatrix" now has up-to-date information.

    sensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

    //Toast.makeText(this, "rotation matrix: " + mRotationMatrix[0] + ", " + mRotationMatrix[1] + ", " + mRotationMatrix[2], Toast.LENGTH_LONG).show();
    //Toast.makeText(this, "orientation matrix: " +  mOrientationAngles[0] + ", " + mOrientationAngles[1] + ", " + mOrientationAngles[2], Toast.LENGTH_LONG).show();

    // "mOrientationAngles" now has up-to-date information.
  }
  /* -------------------------------------------------------
   *
   *  Sensors - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Map orientation functions - start
   *
   ---------------------------------------------------------*/

  //https://stackoverflow.com/questions/37450969/rotate-google-map-to-display-my-real-time-direction
  private void updateCameraBearing(GoogleMap mMap, float bearing) {
    if ( mMap == null) return;
    CameraPosition camPos = CameraPosition
            .builder(
                    mMap.getCameraPosition() // current Camera
            )
            .bearing(bearing)
            .build();
    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(camPos));
  }

  private void updateCamera(float bearing) {
    if ( mMap == null) return;
    CameraPosition oldPos = mMap.getCameraPosition();
    CameraPosition pos = CameraPosition.builder(oldPos).bearing(bearing)
            .build();
    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(pos));

  }

  private void updateCameraAnimate(float bearing) {
    if ( mMap == null) return;

    CameraPosition oldPos = mMap.getCameraPosition();

    CameraPosition pos = CameraPosition.builder(oldPos).bearing(bearing)
            .build();

    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(pos), 100, null);
    //mMap.animateCamera();

  }

  private void updateCameraZoom(ArrayList<Waypoint> wpList, ArrayList<Boolean> wpListSelected, Location myLocation) {




  }

  /* -------------------------------------------------------
   *
   *  Map orientation functions - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Path functions - start
   *
   ---------------------------------------------------------*/

  private void drawNewPath(Marker destination, Location source) {
    //if path already exists, erase it
    if (currentPath != null) {
      currentPath.remove();
      currentPathBackground.remove();
    }
    //draw line
    LatLng pos = destination.getPosition();
    LatLng currentLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
    List<PatternItem> pattern = Arrays.<PatternItem>asList(
            new Gap(2), new Dash(26), new Gap(14) );
    List<PatternItem> patternBackground = Arrays.<PatternItem>asList(
            new Dash(30), new Gap (12) );

    currentPathBackground = mMap.addPolyline(new PolylineOptions()
            .add(pos, currentLatLng).width(12).color(Color.WHITE));

    currentPathBackground.setPattern(patternBackground);

    currentPath = mMap.addPolyline(new PolylineOptions()
            .add(pos, currentLatLng).width(8).color(Color.BLUE));

    currentPath.setPattern(pattern);


    return;
  }

  /* -------------------------------------------------------
   *
   *  Path functions - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  Dialog functions - start
   *
   ---------------------------------------------------------*/



    /* -------------------------------------------------------
   *
   *  Dialog functions - end
   *
   ---------------------------------------------------------*/

}
