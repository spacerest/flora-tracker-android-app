package com.example.spacerest.floratracker.classes;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.utils.CustomUtils;

import java.util.ArrayList;

/**
 * Created by SMP on 3/6/18.
 * With thanks to AbhiAndroid:
 * http://abhiandroid.com/ui/listview
 */

public class WaypointListAdapter extends RecyclerView.Adapter<WaypointListAdapter.ViewHolder> {
    Context context;
    ArrayList<Waypoint> waypointList;
    LayoutInflater inflater;
    boolean[] isCheckedList;// = new boolean[0];
    Waypoints waypointsObj;
    Button btnDel;
    Button btnEdit;
    ViewHolder viewHolder;
    CustomUtils util;

    public WaypointListAdapter(Context applicationContext, ArrayList<Waypoint> waypointList, int size ) {
        this.context = applicationContext;
        this.waypointList = waypointList;
        inflater = (LayoutInflater.from(applicationContext));
        isCheckedList = new boolean[size];
        util = new CustomUtils();

        //this differs from example in tutorial:
        //has this.context = context and inflater = (LayoutInflater.from(applicationContext));
    }

    public boolean itemIsChecked(int position) {
        return isCheckedList[position];
    }

    public void clearCheck(int position) {
        isCheckedList[position] = false;
    }

    @Override
    public long getItemId(int position) {
        return waypointList.get(position).intId;
    }

    @Override
    public int getItemCount() {
        return waypointList.size();
    }

    public void remove(Waypoint wp) {
        waypointList.remove(wp);
    }

    public void updateList(ArrayList<Waypoint> wpList) {
        waypointList = wpList;
    }

    public void edit(Waypoint waypoint, Double wpDetailsLat, Double wpDetailsLong, String wpDetailsTitle, String wpDetailsNotes, int wpColorInt) {
        waypoint.title = wpDetailsTitle;
        waypoint.notes = wpDetailsNotes;
        waypoint.lat = wpDetailsLat;
        waypoint.lng = wpDetailsLong;
        waypoint.colorInt = wpColorInt;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.bindData(waypointList.get(position));

        //in some cases, it will prevent unwanted situations
        holder.checkBox.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        holder.checkBox.setChecked(isCheckedList[position]);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //waypointList.get(holder.getAdapterPosition()).setSelected(isChecked);

                if((buttonView).isChecked()) {
                    isCheckedList[position]=true; }
                else {
                    isCheckedList[position]=false; }
                WaypointListAdapter.this.notifyDataSetChanged();
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,notes,latlng, dateAdded;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.waypointTitleInList);
            notes = (TextView) itemView.findViewById(R.id.waypointDetailInList);
            latlng = (TextView) itemView.findViewById(R.id.waypointLatLongInList);
            dateAdded = (TextView) itemView.findViewById(R.id.waypointDateInList);
            checkBox = (CheckBox) itemView.findViewById(R.id.waypointCbox);

        }

        public void bindData(Waypoint waypoint) {
            String dateAddedStr = waypoint.dateAdded;

            title.setText(waypoint.title.toString());
            notes.setText(waypoint.notes.toString());
            dateAdded.setText(dateAddedStr);//waypointList.get(position).dateAdded.toString());
            latlng.setText(waypoint.lat.toString()+ ", " + waypoint.lng.toString());

        }
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.waypoint_list_view, parent, false);
        return new ViewHolder(v);
    }

    /*
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        //get date in correct format
        String dateAdded = waypointList.get(position).dateAdded;

        if(convertView==null) {
            convertView = inflater.inflate(R.layout.waypoint_list_view, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.waypointTitleInList);
            viewHolder.notes = (TextView) convertView.findViewById(R.id.waypointDetailInList);
            viewHolder.latlng = (TextView) convertView.findViewById(R.id.waypointLatLongInList);
            viewHolder.dateAdded = (TextView) convertView.findViewById(R.id.waypointDateInList);
            viewHolder.checkBox = (CheckBox) convertView.findViewById(R.id.waypointCbox);
            //link the cached views to the convertview
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
            //set the data to be displayed
            viewHolder.title.setText(waypointList.get(position).title.toString());
            viewHolder.notes.setText(waypointList.get(position).notes.toString());
            viewHolder.dateAdded.setText(dateAdded);//waypointList.get(position).dateAdded.toString());
            viewHolder.latlng.setText(waypointList.get(position).lat.toString()+ ", " + waypointList.get(position).lng.toString());

            //VITAL PART!!! Set the state of the
            //CheckBox using the boolean array
            //viewHolder.checkBox.setChecked(true);//isCheckedList[position]);
            viewHolder.checkBox.setSelected(isCheckedList[position]);

            viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()) {
                        isCheckedList[position]=true; }
                    else {
                        isCheckedList[position]=false; }
                    WaypointListAdapter.this.notifyDataSetChanged();
                }
            });
        return convertView;
    }*/

}
