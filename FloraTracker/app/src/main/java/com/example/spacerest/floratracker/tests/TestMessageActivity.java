package com.example.spacerest.floratracker.tests;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.spacerest.floratracker.R;

public class TestMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_message);
    }
}
