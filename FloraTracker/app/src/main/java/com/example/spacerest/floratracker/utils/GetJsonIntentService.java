package com.example.spacerest.floratracker.utils;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

public class GetJsonIntentService extends IntentService {

    private static final String TAG = GetJsonIntentService.class.getSimpleName();

    public static final String PENDING_RESULT_EXTRA = "pending_result";
    public static final String URL_EXTRA = "url";
    public static final String RSS_RESULT_EXTRA = "url";

    public static final int RESULT_CODE = 0;
    public static final int INVALID_URL_CODE = 1;
    public static final int ERROR_CODE = 2;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public GetJsonIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Intent localIntent = new Intent("com.example.spacerest.floratracker.action.GET_JSON");
        localIntent.putExtra("some_key", "this is being returned");
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
/*
public class GetJsonIntentService extends IntentService {

    private static final String TAG = GetJsonIntentService.class.getSimpleName();
    public static final String PENDING_RESULT_EXTRA = "pending_result";
    public static final String URL_EXTRA = "url";
    public static final String JSON_RESULT_EXTRA = "url";
    public WaypointManager wps;

    //error codes
    public static final int RESULT_CODE = 0;
    public static final int INVALID_URL_CODE = 1;
    public static final int ERROR_CODE = 2;

    // TODID: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_GET_JSON = "com.example.spacerest.floratracker.action.GET_JSON";
    private static final String ACTION_SET_JSON = "com.example.spacerest.floratracker.action.SET_JSON";

    // TODID: Rename parameters
    private static final String EXTRA_TEST_JSON_URL = "com.example.spacerest.floratracker.extra.EXTRA_TEST_JSON_URL";
    private static final String EXTRA_JSON_URL = "com.example.spacerest.floratracker.extra.EXTRA_JSON_URL";

    public GetJsonIntentService() {
        super(TAG);


    }


    public static void startActionGetJson(Context context, String url) {
        Intent intent = new Intent(context, GetJsonIntentService.class);
        intent.setAction(ACTION_GET_JSON);
        intent.putExtra(EXTRA_TEST_JSON_URL, url);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        PendingIntent reply = intent.getParcelableExtra(PENDING_RESULT_EXTRA);


        final String action = intent.getAction();
        if (ACTION_GET_JSON.equals(action)) {
            try {
                URL url = new URL(intent.getStringExtra(EXTRA_TEST_JSON_URL));
                InputStreamReader reader = new InputStreamReader(url.openStream());

                JsonParser jp = new JsonParser();

                String jpParsed = jp.parse(reader).toString();

                intent.putExtra(JSON_RESULT_EXTRA, jpParsed); //cumbak
                reply.send(this, RESULT_CODE, intent);

            } catch (MalformedURLException ex) {
                try {
                    reply.send(INVALID_URL_CODE);
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (PendingIntent.CanceledException e) {
                e.printStackTrace();
            }
            final String jsonString = intent.getStringExtra(JSON_RESULT_EXTRA);
            //final String param2 = intent.getStringExtra(EXTRA_PARAM2);
            handleActionGetJson(jsonString);
        }
    }

    private void handleActionGetJson(String jsonString) {
        // TODO: Handle action GetJson
        throw new UnsupportedOperationException("Not yet implemented");
    }



}*/
