package com.example.spacerest.floratracker.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.classes.Waypoint;

import java.util.ArrayList;

/**
 * Created by SMP on 3/16/18.
 */

    public class Manager extends AppCompatActivity {

        ListActivity listActivity = new ListActivity();

        public static final String TEST_MESSAGE = "com.example.spacerest.floratracker.TEST_MESSAGE";
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_manager);
        }

        //open the maps activity
        public void openMaps(View view) {
            Intent intent = new Intent(this, MapsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("waypoints", new ArrayList<Waypoint>());
            intent.putExtras(bundle);
            startActivity(intent);
        }

        public void browseSavedWaypoints(View view) {
            Intent intent = new Intent(this, BrowseWaypointsActivity.class);
            startActivity(intent);
        }

    public void openInfo(View view) {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

        /*test only -- currently testing why header disappears in maps act
        public void testView(View view) {
            Intent intent = new Intent(this, TestMessageActivity.class);
            EditText editText = (EditText) findViewById(R.id.editText);
            String message = editText.getText().toString();
            intent.putExtra(TEST_MESSAGE, message);
            startActivity(intent);
        }*/

    }
