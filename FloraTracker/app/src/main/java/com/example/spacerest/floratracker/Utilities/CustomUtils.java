package com.example.spacerest.floratracker.Utilities;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;

import com.example.spacerest.floratracker.CustomClasses.WaypointManager;
import com.example.spacerest.floratracker.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Created by SMP on 3/10/18.
 */

public class CustomUtils {
    public static String convertStreamToString(InputStream is) throws Exception {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("\n");
            }
            reader.close();
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public String getFormattedDate(String longDateString) {
        String shortDateStr = longDateString;

    /*DateFormat dateFromat = new DateFormat();

    Date date = dateFromat(longDateString);*/

        return shortDateStr;

    }

    public void writeUpdatedJson(String fileName, WaypointManager waypointManager, Context context) {
        // START -- convert all waypointManager to string to update json file
        String jsonStr;
        JavaToJson javaToJson = new JavaToJson(waypointManager);
        jsonStr = javaToJson.getJsonString();
        // END -- convert all waypointManager to string to update json file

        // START -- write json to file
        FileOutputStream outputStream = null;
        try {
            outputStream = context.openFileOutput(fileName, context.MODE_PRIVATE);
            outputStream.write(jsonStr.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // END -- write json to file
    }

    //https://takeoffandroid.com/changing-color-of-drawable-icon-programmatically-bf7396856f5e
    public static Bitmap changeImageColor(Bitmap sourceBitmap, int color) {
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0,
                sourceBitmap.getWidth() - 1, sourceBitmap.getHeight() - 1);
        Paint p = new Paint();
        ColorFilter filter = new LightingColorFilter(0xFFFFFFFF, 0x000000FF );
        p.setColorFilter(filter);

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);
        return resultBitmap;
    }

    public static Drawable convertBitmapToDrawable(Context context, Bitmap bitmap) {
        Drawable d = new BitmapDrawable(context.getResources(), bitmap);
        return d;
    }

    public static Bitmap convertDrawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    public WaypointManager getCurrentWaypoints(String fileName, Context context) {

        String jsonInputString = "";
        FileInputStream is = null;
        String filePath = context.getFilesDir() + "/" + fileName;
        boolean jsonUpdatePossible = false;
        WaypointManager wps;

        //if possible, get json info off the web
        if (jsonUpdatePossible) {

            //temporary string until u get json url and read it
            String testJsonStr = context.getResources().getString(R.string.test_json);

            FileOutputStream outputStream = null;
            try {
                outputStream = context.openFileOutput(fileName, context.MODE_PRIVATE);
                outputStream.write(testJsonStr.getBytes());
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // START read  json file from internal storage
        try {
            is = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            //todo notify user there's currently not a file

            e.printStackTrace();
        }
        try {
            jsonInputString = convertStreamToString(is);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //END read  json file from internal storage

        // START populate json file in internal storage if empty
        File jsonFile = new File(filePath);
        try {
            is = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (jsonInputString.equals("")) {
            wps = new WaypointManager();
            JavaToJson javaToJson = new JavaToJson(wps);
            jsonInputString = javaToJson.getJsonString();
        }
        //END populate json file in internal storage if empty

        // START -- get waypointManager from waypointManager file //
        try {
            JsonToJava jsonToJava = new JsonToJava(jsonInputString);
            wps = jsonToJava.getDownloadedWaypoints();
            return wps;
        } catch (Exception e) {
            e.printStackTrace();
        }
        //END - get waypointManager from waypointManager file //
        return null;
    }

    public int getColorIndex(Context context, int colorInt) {
        int[] colorArray = context.getResources().getIntArray(R.array.color_int_array);
        int colorIndex = 0;
        for(int i = 0; i < colorArray.length; i++) {
            if (colorInt == colorArray[i]) {
                colorIndex = i;
                break;
            }
        }
        return colorIndex;
    }

    public Bitmap getBitmapFromColor(Context context, int colorChosen) {

        Drawable d = context.getResources().getDrawable(R.drawable.spinner_image_blank);
        Drawable d2 = context.getResources().getDrawable(R.drawable.circle_outline);
        Random rnd = new Random();
        int color = -10412708;//Color.argb(255, rnd.nextInt(127) + 127, rnd.nextInt(127) + 127, rnd.nextInt(127) + 127);

        //Convert drawable in to bitmap
        d.setColorFilter(colorChosen, PorterDuff.Mode.SRC_ATOP);
        LayerDrawable ld = new LayerDrawable(new Drawable[] {d, d2});

        Bitmap sourceBitmap = convertDrawableToBitmap(d);

        return sourceBitmap;


    }

}

