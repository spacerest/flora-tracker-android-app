package com.example.spacerest.floratracker.UI;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spacerest.floratracker.CustomClasses.Waypoint;
import com.example.spacerest.floratracker.Models.MapViewModel;
import com.example.spacerest.floratracker.R;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by SMP on 5/24/18.
 */

public class MapManager {
    private String selectedMarkerId;
    private HashMap<String, Marker> markerHashMap = new HashMap<String, Marker>();
    private Context context;
    private MarkerOptions selectedMarkerOptions;
    MapViewModel viewModel;
    public GoogleMap mMap;
    public Location lastLocation;
    public Marker userLocationMarker;
    public MarkerOptions userLocationMarkerOptions;

    //path variables
    //variables: draw path
    Polyline currentPath = null;
    Polyline currentPathBackground = null;
    Marker selectedMarker = null;

    //list variables
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    public WaypointListAdapter wpListAdapter;

    //marker-dependant buttons
    ImageButton btnDelete;
    ImageButton bntEdit;

    View parentView;

    int[] colorArray;

    //sensors
    public SensorManager mSensorManager;
    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];
    double lastBearing;
    double previousOrientation = 0.0;
    CameraPosition currentPos = null;

    int mapWidthPx;
    int mapHeightPx;


    public WaypointListAdapter getWpListAdapter() {
        return wpListAdapter;
    }

    public MapManager(final Context context, RecyclerView mRecyclerView, View inheritedParentView) {
        viewModel = new MapViewModel();
        this.context = context;

         userLocationMarkerOptions = new MarkerOptions();

        viewModel = new MapViewModel();
        viewModel.init(context);
        this.mRecyclerView = mRecyclerView;

        parentView = inheritedParentView;


        btnDelete = (ImageButton) parentView.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(listenerDelete);
        bntEdit = (ImageButton) parentView.findViewById(R.id.bntEdit);
        bntEdit.setOnClickListener(listenerEdit);

        colorArray = context.getResources().getIntArray(R.array.color_int_array);
        selectedMarkerOptions = new MarkerOptions();

        mRecyclerView.addItemDecoration(new DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL));
        wpListAdapter = new WaypointListAdapter(context, viewModel.getWaypointList(), viewModel.getWaypointCount());
        wpListAdapter.notifyDataSetChanged();

        //setInitialZoom();

        wpListAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver()
        {
            @Override
            public void onChanged() {

                //int itemCount = viewModel.getWaypointCount();
                int itemCount = wpListAdapter.getItemCount();

                int checkCount = 0;

                for (int i = 0; i <= itemCount - 1; i++) { //itemCount - 1; i >= 0; i--) {
                    String listItemId = wpListAdapter.getWaypointId(i);
                    Marker listItemMarker = getMarkerById(listItemId);
                    Boolean boo = wpListAdapter.itemIsChecked(i);
                    listItemMarker.setVisible(boo);

                    if (wpListAdapter.itemIsChecked(i)) {
                        checkCount++;
                        updateSelectedWaypoint(getWaypointById(listItemId));

                    } else if (!wpListAdapter.itemIsChecked(i)) {
                        if (checkCount > 0) {
                            checkCount--;
                        }
                        if (selectedMarkerId == listItemId) {
                            removeSelectedWaypoint();
                        }
                    }

                    if(checkCount == 1) {
                        bntEdit.setEnabled(true);
                        btnDelete.setEnabled(true);
                        bntEdit.setAlpha(255);
                        btnDelete.setAlpha(255);
                    } else if (checkCount > 1) {
                        bntEdit.setEnabled(false);
                        btnDelete.setEnabled(true);
                        bntEdit.setAlpha(100);
                        btnDelete.setAlpha(255);
                    } else if (checkCount == 0) {
                        bntEdit.setEnabled(false);
                        btnDelete.setEnabled(false);
                        bntEdit.setAlpha(100);
                        btnDelete.setAlpha(100);
                    }
                }
                setInitialZoom();
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(wpListAdapter);
    }

    public void setInitialZoom() {
        //update the zoom to include all the currently selected markers - start
        //https://stackoverflow.com/questions/14828217/android-map-v2-zoom-to-show-all-the-markers

        mapWidthPx =  parentView.getMeasuredWidth();
        mapHeightPx =  parentView.getMeasuredHeight() - (56 * 2);

        if (this.mMap != null && lastLocation != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (String wpId : markerHashMap.keySet()) {
                if (wpListAdapter.itemIsChecked(wpListAdapter.getPositionById(wpId))) {
                    builder.include(markerHashMap.get(wpId).getPosition());
                }
            }

            //include our current location in the bounds
            builder.include(new LatLng( lastLocation.getLatitude(), lastLocation.getLongitude()));

            LatLngBounds bounds = builder.build();

            LatLng center = bounds.getCenter();
            float zoom = getBoundsZoomLevel(bounds, this.mapWidthPx, this.mapHeightPx);

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(center)
                    .zoom(zoom - 2)
                    .bearing((float) this.lastBearing)
                    .build();
            this.mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void updateLocation(Location location) {
        lastLocation = location;
        drawUserLocationMarker();
        drawNewPath();
        setDistanceText();
    }

    public float getDistanceFromSelectedMarker(Location currentLocation) {
            //get distance
        Location temp = new Location(LocationManager.GPS_PROVIDER);
        temp.setLatitude(selectedMarker.getPosition().latitude);
        temp.setLongitude(selectedMarker.getPosition().longitude);
            return currentLocation.distanceTo(temp);

    }

    public void setDistanceText() {
        if (lastLocation != null && selectedMarker != null) {
            DecimalFormat df = new DecimalFormat("#.00");
            float distance = getDistanceFromSelectedMarker(lastLocation);
            float distanceToShow = Float.valueOf(df.format(distance));
            TextView mLatLongText = (TextView) parentView.findViewById(R.id.latLongText);
            mLatLongText.setText(
                    context.getResources().getString(R.string.lat_long_text,
                            distanceToShow
                    ));
        }
    }

    private void drawUserLocationMarker() {

        if (userLocationMarker != null) {
            userLocationMarker.remove();
        }
        LatLng latlng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
        userLocationMarkerOptions.position(latlng);
        //markerOptions.snippet(wp.notes);

        userLocationMarkerOptions.icon(getMarkerIconFromDrawable(Color.BLUE));
        userLocationMarkerOptions.anchor(0.5f, 0.5f);
        userLocationMarkerOptions.isDraggable();

        userLocationMarker = mMap.addMarker(userLocationMarkerOptions);

    }

    public void deleteWaypoint(String id) {
        wpListAdapter.removeCheckFromHolder(id);
        viewModel.delete(id);
        markerHashMap.get(id).remove();
        markerHashMap.remove(id);
        if (selectedMarkerId == id) {
            selectedMarker.remove();
            selectedMarkerId = null;
            currentPath.remove();
            currentPathBackground.remove();

        }
        wpListAdapter.notifyDataSetChanged();
    }

    public void addMarkerToHolder(Waypoint waypoint, Marker marker) {
        markerHashMap.put(waypoint.id, marker);
        updateSelectedMarkerOptions(waypoint);
        wpListAdapter.notifyDataSetChanged();
        marker.setVisible(false);
    }

    public MarkerOptions updateSelectedMarkerOptions(Waypoint waypointToMatch) {

        selectedMarkerId = waypointToMatch.id;
        LatLng pos = new LatLng( waypointToMatch.lat, waypointToMatch.lng);
        selectedMarkerOptions.position(pos);
        selectedMarkerOptions.icon(getSelectedMarkerFromDrawable());

        return selectedMarkerOptions;
    }

    public void deleteSelectedWaypoints() {

        int itemCount = wpListAdapter.getItemCount();

        ArrayList<String> idsToDelete = new ArrayList<String>();

        for (int i = 0; i <= itemCount - 1; i++) { //itemCount - 1; i >= 0; i--) {
            String wpId = wpListAdapter.getWaypointId(i);
            Boolean boo = wpListAdapter.itemIsChecked(i);

            if (boo) {idsToDelete.add(wpId); }
        }

        for (int i = 0; i < idsToDelete.size(); i++) {
            deleteWaypoint(idsToDelete.get(i));
        }
    }

    public MarkerOptions addWaypoint(Double lat, Double lng, String title, String notes, int colorInt){
        viewModel.add(lat, lng, title, notes, colorInt);
        return setMarkerOptions(viewModel.getLastAddedWaypoint());

    }

    public Waypoint getWaypointById(String id) {
        return viewModel.getWaypointById(id);
    }

    public Marker getSelectedMarker() {
        return selectedMarker;
    }

    public void showMarker(Marker marker) {
        marker.setVisible(true);
    }

    public Waypoint getWaypointByMarker(Marker marker) {
        Iterator it = markerHashMap.entrySet().iterator();
        String waypointId = null;

        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();
            Marker matchingMarker = (Marker) pair.getValue();
            waypointId = (String) pair.getKey();
            if (matchingMarker.equals(marker)) {
                break;
            }
        }
        return getWaypointById(waypointId);
    }

    public String getSelectedMarkerId(){
        return selectedMarkerId;
    }

    public Marker getMarkerById(String searchId){
        Iterator it = markerHashMap.entrySet().iterator();
        Marker matchingMarker = null;

        while (it.hasNext()) {
            HashMap.Entry pair = (HashMap.Entry) it.next();
            matchingMarker = (Marker) pair.getValue();
            String id = (String) pair.getKey();
            if (id == searchId) {
                break;
            }
        }
        return matchingMarker;
    }

    public MarkerOptions setMarkerOptions(Waypoint wp) {
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latlng = new LatLng(wp.lat, wp.lng);
        markerOptions.position(latlng);
        markerOptions.title(wp.title);

        markerOptions.icon(getMarkerIconFromDrawable(wp.colorInt));
        markerOptions.anchor(0.5f, 0.5f);
        markerOptions.isDraggable();
        return markerOptions;
    }

    private BitmapDescriptor getSelectedMarkerFromDrawable() {
        Drawable sproutD = context.getResources().getDrawable(R.drawable.sprout_marker);
        Bitmap sourceBitmap = convertDrawableToBitmap(sproutD);
        return BitmapDescriptorFactory.fromBitmap(sourceBitmap);
    }

    private BitmapDescriptor getUserLocationIconFromDrawable() {

        Drawable d = context.getResources().getDrawable(R.drawable.circle_marker);
        d.setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_ATOP);
        Bitmap sourceBitmap = convertDrawableToBitmap(d);

        return BitmapDescriptorFactory.fromBitmap(sourceBitmap);

    }


    private BitmapDescriptor getMarkerIconFromDrawable(int colorChosen) {

        Drawable d = context.getResources().getDrawable(R.drawable.circle_marker);
        Drawable d2 = context.getResources().getDrawable(R.drawable.circle_outline);

        //Convert drawable in to bitmap
        d.setColorFilter(colorChosen, PorterDuff.Mode.SRC_ATOP);
        LayerDrawable ld = new LayerDrawable(new Drawable[] {d, d2});

        Bitmap sourceBitmap = convertDrawableToBitmap(ld);

        return BitmapDescriptorFactory.fromBitmap(sourceBitmap);
    }

    public static Bitmap convertDrawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

      /* -------------------------------------------------------
   *
   *  Check mark listeners - start
   *
   ---------------------------------------------------------*/



    View.OnClickListener listenerEdit = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

        }
    };

    View.OnClickListener listenerDelete = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            final View dialogView = inflater.inflate(R.layout.default_dialog, null);

            alertDialogBuilder.setView(dialogView);

            TextView dialogText = (TextView) dialogView.findViewById(R.id.default_dialog_content);
            dialogText.setText("Delete this waypoint permanently?");

            AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    deleteSelectedWaypoints();
                    //markerHolder.getSelectedMarker().remove();
                    //markerHolder.getMarkerById(markerHolder.getSelectedMarkerId()).remove();
                    //markerHolder.deleteWaypoint(markerHolder.getSelectedMarkerId());
                    //currentPath.remove();
                    //currentPathBackground.remove();
                }
            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                }
            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();

        }
    };

    /* -------------------------------------------------------
   *
   *  Check mark listeners - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  Marker functions - start
   *
   ---------------------------------------------------------*/

    public void sendWaypointToMarker(Double wpDetailsLat, Double wpDetailsLong, String wpDetailsTitle, String wpDetailsNotes, int wpColorInt) {

        wpListAdapter.addCheckToHolder();
        MarkerOptions markerOptions = addWaypoint(wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);
        Marker newMarker = mMap.addMarker(markerOptions);
        addMarkerToHolder(viewModel.getLastAddedWaypoint(), newMarker);
        updateSelectedWaypoint(viewModel.getLastAddedWaypoint());
        showMarker(newMarker);

    }

    //TODO combine updateselectedmarkeroptions into new function updateSelectedMarker
    public void updateSelectedWaypoint(Waypoint waypoint) {
        //TODO think about which waypoint is getting shown as selected
        Marker oldSelectedMarker = getSelectedMarker();
        if (oldSelectedMarker != null) {
            oldSelectedMarker.remove();
        }

        MarkerOptions selectedMarkerOptions = updateSelectedMarkerOptions(waypoint);
        Marker newMarker = mMap.addMarker(selectedMarkerOptions);
        selectedMarker = newMarker;
        drawNewPath();
        setDistanceText();


    }

    public void removeSelectedWaypoint() {
        Marker oldSelectedMarker = getSelectedMarker();
        if (oldSelectedMarker != null) {
            oldSelectedMarker.remove();
        }

        selectedMarker = null;
        selectedMarkerId = null;
        drawNewPath();
    }


  /* -------------------------------------------------------
   *
   *  Markers - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  Path functions - start
   *
   ---------------------------------------------------------*/



    public void drawNewPath() {
        //if path already exists, erase it
        if (currentPath != null) {
            currentPath.remove();
            currentPathBackground.remove();
        }

        if (selectedMarker != null && lastLocation != null) {
            //draw line
            LatLng pos = getSelectedMarker().getPosition();
            LatLng currentLatLng = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
            List<PatternItem> pattern = Arrays.<PatternItem>asList(
                    new Gap(2), new Dash(26), new Gap(14));
            List<PatternItem> patternBackground = Arrays.<PatternItem>asList(
                    new Dash(30), new Gap(12));

            currentPathBackground = mMap.addPolyline(new PolylineOptions()
                    .add(pos, currentLatLng).width(12).color(Color.WHITE));

            currentPathBackground.setPattern(patternBackground);

            currentPath = mMap.addPolyline(new PolylineOptions()
                    .add(pos, currentLatLng).width(8).color(Color.BLUE));

            currentPath.setPattern(pattern);
        }

        return;
    }

  /* -------------------------------------------------------
   *
   *  Path functions - end
   *
   ---------------------------------------------------------*/

  /*
   * Sensor functions - start
   */

  public double logSensorChanged(SensorEvent event) {
      if (mMap != null && lastLocation != null) {
          //lastBearing = lastLocation.getBearing();
      }

      //Toast.makeText(this, "sensor changed", Toast.LENGTH_LONG).show();
      if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
          System.arraycopy(event.values, 0, mAccelerometerReading,
                  0, mAccelerometerReading.length);
          updateOrientationAngles();

      }
      else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
          System.arraycopy(event.values, 0, mMagnetometerReading,
                  0, mMagnetometerReading.length);
          updateOrientationAngles();

      }

      if ((mMap != null && lastLocation != null)) {
          if (Math.abs(Math.toDegrees(mOrientationAngles[0]) - Math.toDegrees(previousOrientation)) > 1.0) {
              float bearing = (float) Math.toDegrees(mOrientationAngles[0]);// + mDeclination;
              previousOrientation = mOrientationAngles[0];
              CameraPosition oldPos = mMap.getCameraPosition();
              currentPos = CameraPosition.builder(oldPos).bearing(bearing)
                      .build();
              mMap.animateCamera(CameraUpdateFactory.newCameraPosition(currentPos));
          }
      }
      this.lastBearing = Math.toDegrees(previousOrientation);
      return this.lastBearing;
  }

    // Compute the three orientation angles based on the most recent readings from
    // the device's accelerometer and magnetometer.
    public void updateOrientationAngles() {

        // Update rotation matrix, which is needed to update orientation angles.
        mSensorManager.getRotationMatrix(mRotationMatrix, null,
                mAccelerometerReading, mMagnetometerReading);

        // "mRotationMatrix" now has up-to-date information.

        mSensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

        // "mOrientationAngles" now has up-to-date information.
    }

    /*
   * Sensor functions - end
   */

    /* Latlngbounds - get right initial view - start
     */

    private double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double p = 0.017453292519943295;    // Math.PI / 180
        double a = 0.5 - Math.cos((lat2 - lat1) * p)/2 +
                Math.cos(lat1 * p) * Math.cos(lat2 * p) *
                        (1 - Math.cos((lon2 - lon1) * p))/2;

        return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
    }

    private void getInitialBoundsWithZoom(LatLngBounds fitAllMarkers, double bearingOffNorth) {

        Double neLat = fitAllMarkers.northeast.latitude;
        Double  neLong = fitAllMarkers.northeast.longitude;
        Double swLat = fitAllMarkers.southwest.latitude;
        Double swLong = fitAllMarkers.southwest.longitude;
        LatLng ne = new LatLng (fitAllMarkers.northeast.latitude, fitAllMarkers.northeast.longitude);
        LatLng sw = new LatLng (fitAllMarkers.southwest.latitude, fitAllMarkers.southwest.longitude);

        double centerLat = fitAllMarkers.getCenter().latitude;
        double centerLong = fitAllMarkers.getCenter().longitude;



        double C = Math.abs(neLat - swLat) / 2; //TODO does this need to be different at different places?
        double B = Math.abs(neLong - swLong) / 2;

        double A = Math.sqrt((B*B) + (C*C));

        double xFromCenter = A * Math.cos(Math.asin(C/A) - Math.toRadians(bearingOffNorth));

        double yFromCenter = (B/C) * xFromCenter;

        //double metersPerPx = 156543.03392 * Math.cos(fitAllMarkers.getCenter().latitude * Math.PI / 180) / Math.pow(2, zoom);

        double distance = getDistance(centerLat - yFromCenter, centerLong - xFromCenter, centerLat + yFromCenter, centerLong + xFromCenter);

        /*
        20 : 1128.497220
        19 : 2256.994440
        18 : 4513.988880
        17 : 9027.977761
        16 : 18055.955520
        15 : 36111.911040
        14 : 72223.822090
        13 : 144447.644200
        12 : 288895.288400
        11 : 577790.576700
        10 : 1155581.153000
        9  1: 2311162.307000
        8  1: 4622324.614000
        7  1: 9244649.227000
        6  1: 18489298.450000
        5  1: 36978596.910000
        4  1: 73957193.820000
        3  1: 147914387.600000
        2  1: 295828775.300000
        1  1: 591657550.500000
         */


    }

    private double getXDistanceFromCenter(double B, double C, double theta, LatLng center) {
        double A;
        double xFromCenter;
        double yFromCenter;

        A = Math.sqrt((B*B) + (C*C));

        xFromCenter = A * Math.cos(Math.asin(C/A) - theta);
        double y = A * Math.cos(Math.asin(C/A) - theta);
        yFromCenter = (C/B) * xFromCenter;

        return xFromCenter;
    }

    private static final double LN2 = 0.6931471805599453;
    private static final int WORLD_PX_HEIGHT = 256;
    private static final int WORLD_PX_WIDTH = 256;
    private static final int ZOOM_MAX = 21;

    public int getBoundsZoomLevel(LatLngBounds bounds, int mapWidthPx, int mapHeightPx){

        LatLng ne = bounds.northeast;
        LatLng sw = bounds.southwest;

        double latFraction = (latRad(ne.latitude) - latRad(sw.latitude)) / Math.PI;

        double lngDiff = ne.longitude - sw.longitude;
        double lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        double latZoom = zoom(mapHeightPx, WORLD_PX_HEIGHT, latFraction);
        double lngZoom = zoom(mapWidthPx, WORLD_PX_WIDTH, lngFraction);

        int result = Math.min((int)latZoom, (int)lngZoom);
        return Math.min(result, ZOOM_MAX);
    }

    private double latRad(double lat) {
        double sin = Math.sin(lat * Math.PI / 180);
        double radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
        return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
    }
    private double zoom(int mapPx, int worldPx, double fraction) {
        return Math.floor(Math.log(mapPx / worldPx / fraction) / LN2);
    }


    /* Latlngbounds - get right initial view - end
     */


}

