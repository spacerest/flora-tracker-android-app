package com.example.spacerest.floratracker.Utilities;
import com.example.spacerest.floratracker.CustomClasses.WaypointManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by SMP on 1/10/18.
 */

public class JavaToJson {

    private String wpString;

    public JavaToJson(WaypointManager wps) {

        wpString = new Gson().toJson(wps, WaypointManager.class);
    }

    public static void main(String[] args) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        /*
        Waypoint wp = new Waypoint(94.0, 94.0, "json wp", "test json wp");

        System.out.println(gson.toJson(wp)); */

        /*
        WaypointManager wps = new WaypointManager();
        wps.title = "List of waypointManager";
        wps.notes = "These are test waypointManager";
        wps.total = 5;
        wps.limit = 10;
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        System.out.println(gson.toJson(wps));
        */

    }

    public String getJsonString() {
        return wpString;
    }
}
