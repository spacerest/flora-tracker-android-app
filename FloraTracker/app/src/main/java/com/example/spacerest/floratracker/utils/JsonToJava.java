package com.example.spacerest.floratracker.utils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.activities.BrowseWaypointsActivity;
import com.example.spacerest.floratracker.activities.MapsActivity;
import com.example.spacerest.floratracker.classes.Waypoints;
import com.example.spacerest.floratracker.tests.TestMessageActivity;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;

/**
 * Created by spacerest on 1/16/18.
 */

public class JsonToJava {

    public JsonToJava(String jsonStr) throws IOException {
        //url = new URL(jsonUrl);
        //reader = new InputStreamReader(jsonUrl.openStream());
        /*String jsonStr =
                "{ 'title': 'List of flora tracker waypointManager'," +
                        "'notes': 'These are the waypointManager used during testing this app'," +
                        "'errors': []," +
                        "'total': 0," +
                        "'limit': 20," +
                        "'points':" +
                        "[{" +
                        "'id': 1," +
                        "'title': 'test waypoint 1'," +
                        "'dateAdded': '01/28/18'," +
                        "'lng': -87.6508213," +
                        "'lat': 41.8378638," +
                        "'notes': 'test wp notes'" +
                        "},{" +
                        "'id': 2," +
                        "'title': 'test waypoint 2'," +
                        "'dateAdded': '01/28/18'," +
                        "'lng': -87.6508213," +
                        "'lat': 41.8378638," +
                        "'notes': 'test wp notes'" +
                        "},{" +
                        "'id': 3," +
                        "'title': 'test waypoint 3'," +
                        "'dateAdded': '01/28/18'," +
                        "'lng': -87.6508213," +
                        "'lat': 41.8378638," +
                        "'notes': 'test wp notes'" +
                        "}]" +
                        "}";*/
        //wps = new Gson().toJson(jsonStr);
        wps2 = new Gson().fromJson(jsonStr, Waypoints.class);
    }

    InputStreamReader reader;
    //WaypointManager wps;
    Waypoints wps2;

    public Waypoints getDownloadedWaypoints() {
        return wps2;
    }

    public static void main(String[] args) throws Exception {

    }


}
