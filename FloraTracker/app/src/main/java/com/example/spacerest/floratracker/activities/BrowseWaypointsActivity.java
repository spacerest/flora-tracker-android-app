package com.example.spacerest.floratracker.activities;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.classes.ColorData;
import com.example.spacerest.floratracker.classes.ImageSpinnerAdapter;
import com.example.spacerest.floratracker.classes.Waypoint;
import com.example.spacerest.floratracker.classes.WaypointListAdapter;
import com.example.spacerest.floratracker.classes.Waypoints;
import com.example.spacerest.floratracker.utils.CustomUtils;

import java.util.ArrayList;

import android.view.View.OnClickListener;

//how to mock up json http://michael-kuehnel.de/api/2016/11/04/data-mocking-ways-to-fake-a-backend-api.html

public class BrowseWaypointsActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

  SharedPreferences sharedPref;
  int locationCount;
  Waypoints wps;
  int sortChoice = -1;
  int waypointCount;
  ListActivity listActivity = new ListActivity();
  boolean waypointWasEdited = false;
  CustomUtils utils = new CustomUtils();
  WaypointListAdapter wpListAdapter;
  ImageButton btnDel;
  ImageButton btnEdit;
  ImageButton btnGoToMap;
  ArrayList<Waypoint> wpList = new ArrayList<Waypoint>();
  String fileName;
  ArrayList<Boolean> wpListSelected = new ArrayList<Boolean>();
  int[] colorArray;


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    /* -------------------------------------------------------
   *
   *  Life cycle functions - start
   *
   ---------------------------------------------------------*/
    //get existing info from maps activity class
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            wpListSelected = (ArrayList<Boolean>) intent.getExtras().getSerializable("selected");

            wps.points = (ArrayList<Waypoint>) intent.getExtras().getSerializable("waypointManager");

            wpListAdapter.updateList(wps.points);

            //mark the correct waypointManager as checked


        } catch (Exception e) {
            e.printStackTrace();
        }
        wpListAdapter.notifyDataSetChanged();
    }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_browse_waypoints);
    fileName = getResources().getString(R.string.json_file_name);
    colorArray = getResources().getIntArray(R.array.color_int_array);

    //find buttons and set initial state
      btnDel = (ImageButton) findViewById(R.id.btnDel);
      btnEdit = (ImageButton) findViewById(R.id.btnEdit);
      btnGoToMap = (ImageButton) findViewById(R.id.btnMap);
      btnEdit.setEnabled(false);
      btnEdit.setAlpha(100);
      btnDel.setEnabled(false);
      btnDel.setAlpha(100);

    //get list of current waypointManager in json file (or off the web, if possible)
      wps = utils.getCurrentWaypoints(fileName, this);
      waypointCount = wps.points.size();
      mRecyclerView = (RecyclerView)findViewById(R.id.waypointListView);
      wpListAdapter = new WaypointListAdapter(getApplicationContext(), wps.points, waypointCount);
      wpListAdapter.notifyDataSetChanged();

      wpListAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver()
      {
          @Override
          public void onChanged()
          {
              int itemCount = wps.points.size();

              int checkCount = 0;

              //SparseBooleanArray checkedItemPositions = listView.getCheckedItemPositions();
              for(int i=itemCount-1; i >= 0; i--){
                  if (wpListAdapter.itemIsChecked(i)) {
                      checkCount++;
                  } else if (wpListAdapter.itemIsChecked(i) && checkCount > 0) {
                      checkCount--;
                  }
              }
              if(checkCount == 1) {
                  btnEdit.setEnabled(true);
                  btnDel.setEnabled(true);
                  btnEdit.setAlpha(255);
                  btnDel.setAlpha(255);
              } else if (checkCount > 1) {
                  btnEdit.setEnabled(false);
                  btnDel.setEnabled(true);
                  btnEdit.setAlpha(100);
                  btnDel.setAlpha(255);
              } else if (checkCount == 0) {
                  btnEdit.setEnabled(false);
                  btnDel.setEnabled(false);
                  btnEdit.setAlpha(100);
                  btnDel.setAlpha(100);
              }
          }
      });

      btnDel.setOnClickListener(listenerDelete);
    btnEdit.setOnClickListener(listenerEdit);
    btnGoToMap.setOnClickListener(listenerGoToMap);
    mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    mRecyclerView.setAdapter(wpListAdapter);
  }

  /* -------------------------------------------------------
   *
   *  Life cycle functions - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Check mark listeners - start
   *
   ---------------------------------------------------------*/

    OnClickListener listenerDelete = new OnClickListener() {
        @Override
        public void onClick(View v) {
            /** Getting the checked items from the listview */

            int itemCount = wps.points.size();

            for(int i=itemCount-1; i >= 0; i--){
                if (wpListAdapter.itemIsChecked(i)) {
                    //clear check so that waypoint below doesn't inherit the check?
                    wpListAdapter.clearCheck(i);
                    wpListAdapter.remove(wps.points.get(i));
                    waypointWasEdited = true;
                }
            }
            if (waypointWasEdited) {
                wpListAdapter.notifyDataSetChanged();
                waypointWasEdited = false;
                utils.writeUpdatedJson(fileName, wps, BrowseWaypointsActivity.this);
            }
        }
    };

    OnClickListener listenerEdit = new OnClickListener() {
        @Override
        public void onClick (View view) {
            int itemCount = wps.points.size();

            for(int i=itemCount-1; i >= 0; i--){
                final int currentWp = i;
                if (wpListAdapter.itemIsChecked(i)) {
                    waypointWasEdited = true;

                    //https://stackoverflow.com/a/5763969/5650506
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BrowseWaypointsActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.waypoint_details_dialog, null);
                    final String filePath = getFilesDir() + "/" + fileName;

                    // set prompts.xml to alertdialog builder
                    alertDialogBuilder.setView(dialogView);
                    alertDialogBuilder.setTitle("Edit");
                    EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);
                    EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);
                    EditText etTitle = (EditText) dialogView.findViewById(R.id.waypoint_detail_title_id);
                    EditText etNotes = (EditText) dialogView.findViewById(R.id.waypoint_detail_notes_id);

                    ArrayList<ColorData> colorDataList = new ArrayList<ColorData>();

                    for (int j = 0; j < colorArray.length; j++) {
                        colorDataList.add(new ColorData(j));
                    }

                    final Spinner colorSpinner = (Spinner) dialogView.findViewById(R.id.color_spinner);

                    ImageSpinnerAdapter spinnerAdapter = new ImageSpinnerAdapter(BrowseWaypointsActivity.this, R.layout.image_spinner_row,
                            R.id.spinnerImage, colorDataList);

                    colorSpinner.setAdapter(spinnerAdapter);

                    if (wps.points.get(i).colorInt != 0) {
                        int colorIndex = utils.getColorIndex(BrowseWaypointsActivity.this, wps.points.get(i).colorInt);
                        colorSpinner.setSelection(colorIndex);
                    } else {
                        colorSpinner.setSelection(0);
                    }

                    //colorSpinner.setSelection(4);
                    //colorSpinner
                    String lat = String.valueOf(wps.points.get(i).lat);
                    String lng = String.valueOf(wps.points.get(i).lng);
                    String notes = String.valueOf(wps.points.get(i).notes);
                    String title = String.valueOf(wps.points.get(i).title);

                    etLong.setText(lng, TextView.BufferType.EDITABLE);
                    etLat.setText(lat, TextView.BufferType.EDITABLE);
                    etNotes.setText(notes, TextView.BufferType.EDITABLE);
                    etTitle.setText(title, TextView.BufferType.EDITABLE);
                    //colorSpinner.setSelection(colorArray[utils.getColorIndex(BrowseWaypointsActivity.this,
                            //wps.points.get(i).colorInt)]);

                    AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);
                            EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);
                            EditText etTitle = (EditText) dialogView.findViewById(R.id.waypoint_detail_title_id);
                            EditText etNotes = (EditText) dialogView.findViewById(R.id.waypoint_detail_notes_id);

                            String wpDetailsTitle = etTitle.getText().toString();
                            String wpDetailsNotes = etNotes.getText().toString();
                            Double wpDetailsLat = Double.parseDouble(etLat.getText().toString());
                            Double wpDetailsLong = Double.parseDouble(etLong.getText().toString());
                            int wpColorInt = colorArray[colorSpinner.getSelectedItemPosition()];

                            Toast.makeText(BrowseWaypointsActivity.this, "Updated", Toast.LENGTH_SHORT).show();

                            wpListAdapter.edit(wps.points.get(currentWp), wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);

                            if (waypointWasEdited) {
                                wpListAdapter.clearCheck(currentWp);
                                wpListAdapter.notifyDataSetChanged();
                                waypointWasEdited = false;
                                utils.writeUpdatedJson(fileName, wps, BrowseWaypointsActivity.this);
                            }
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            wpListAdapter.clearCheck(currentWp);
                            wpListAdapter.notifyDataSetChanged();
                            Toast.makeText(BrowseWaypointsActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
                        }
                    });

                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    break;
                }
            }
        }


    };

    OnClickListener listenerGoToMap = new OnClickListener() {
        @Override
        public void onClick(View v) {
            /** Getting the checked items from the listview */

            int itemCount = wps.points.size();
            ArrayList<Waypoint> waypointList = new ArrayList<Waypoint>();
            ArrayList<Boolean> selectedList = new ArrayList<Boolean>();

            for(int i=0; i < itemCount; i++){
                waypointList.add(wps.points.get(i));

                if (wpListAdapter.itemIsChecked(i)) {
                    selectedList.add(true);
                } else {
                    selectedList.add(false);
                }
            }

            Intent intent = new Intent(BrowseWaypointsActivity.this, MapsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("waypointManager", waypointList);
            bundle.putSerializable("selected", selectedList);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

  /* -------------------------------------------------------
   *
   *  Check mark listeners - end
   *
   ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  User nav actions functions - start
   *
   ---------------------------------------------------------*/

    public void openMaps(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivity(intent);
    }

    public void openHome(View view) {
        Intent intent = new Intent(this, Manager.class);
        startActivity(intent);
    }

    public void searchWaypoints(View view) {

        //https://stackoverflow.com/a/5763969/5650506
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.search_dialog, null);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(dialogView);

        alertDialogBuilder.setTitle(R.string.search_dialog_title);

        // set dialog message
        AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Search", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //search waypointManager
                EditText etSearchTerm = (EditText) dialogView.findViewById(R.id.search_waypoints_text);
                String strSearchTerm = etSearchTerm.getText().toString();
                Toast.makeText(BrowseWaypointsActivity.this, strSearchTerm, Toast.LENGTH_SHORT).show();

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(BrowseWaypointsActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public void sortWaypoints(View view) {

        //https://stackoverflow.com/a/5763969/5650506
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.sort_dialog, null);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(dialogView);

        alertDialogBuilder.setTitle(R.string.sort_dialog_title);

        Spinner spinner = (Spinner) dialogView.findViewById(R.id.sort_spinner);

        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.sort_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);

        // set dialog message
        AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Sort", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //sort waypointManager
                switch(sortChoice) {
                    case 0: sortWaypointsByName();
                        break;

                    case 1: sortWaypointsByDate();
                        break;

                    case 2: sortWaypointsByDistance();
                        break;
                }

            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(BrowseWaypointsActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

        spinner.setOnItemSelectedListener(this);

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

  /* -------------------------------------------------------
   *
   *  User nav actions functions - end
   *
   ---------------------------------------------------------*/

  public void sortWaypointsByDate() {
    Toast.makeText(BrowseWaypointsActivity.this, "by date", Toast.LENGTH_SHORT).show();
  }

  public void sortWaypointsByName() {
    Toast.makeText(BrowseWaypointsActivity.this, "by name", Toast.LENGTH_SHORT).show();

  }

  public void sortWaypointsByDistance() {
    Toast.makeText(BrowseWaypointsActivity.this, "by distance", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    sortChoice = position;
  }

  @Override
  public void onNothingSelected(AdapterView<?> parent) {
  }

}
