package com.example.spacerest.floratracker.UI;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.spacerest.floratracker.CustomClasses.Waypoint;
import com.example.spacerest.floratracker.classes.ColorData;
import com.example.spacerest.floratracker.classes.ImageSpinnerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import com.example.spacerest.floratracker.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by SMP on 5/16/18.
 */



public class MainActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationButtonClickListener, SensorEventListener {

    //used for json download codes (use later)
    private static final int JSON_DOWNLOAD_REQUEST_CODE = 0;

    //used for getting permission to access user's location
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    //variables: map
    //private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    Location mLastLocation;
    boolean mapIsConnected = false;
    SupportMapFragment mapFragment;

    int[] colorArray;

    //buttons
    ImageButton btnAddNew;
    ImageButton btnHome1;
    ImageButton btnGoToList;
    ImageButton btnGoToMap;
    ImageButton btnHome2;

    //views
    View mLoadingView;
    boolean loadingViewVisible;
    private DrawerLayout drawerLayout;
    private RecyclerView mRecyclerView;
    private TextView mLatLongText;
    private MapManager mapManager;
    private MapState mapState;
    private LocationCallback mLocationCallback;
    FusedLocationProviderClient mFusedLocationClient;

    //sensors
    private SensorManager mSensorManager;
    double previousAccuracy = -1.0;


    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setupViews();


        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);

        mapFragment.getMapAsync(this);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        loadingViewVisible = true;

        mapManager = new MapManager(this, mRecyclerView, drawerLayout);
        mapManager.mSensorManager = (SensorManager) getSystemService(this.SENSOR_SERVICE);

        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                //setTheme(R.style.AppTheme);
                mLastLocation = locationResult.getLastLocation();
                mapManager.updateLocation(mLastLocation);
                updateDistanceText(mLastLocation);

                if (loadingViewVisible) {
                    mapManager.setInitialZoom();
                    mLoadingView.setVisibility(View.GONE);
                    loadingViewVisible = false;
                    mapManager.setInitialZoom();
                }
            }
        };
    }

    private void updateDistanceText(Location newLocation) {
        //tell the user how far away we are from selected marker
        if (mapManager.getSelectedMarker() != null) {
            mLatLongText.setText(
                    getString(R.string.lat_long_text,
                            mapManager.getDistanceFromSelectedMarker(newLocation)
                    ));
        }
    }

    private void setupViews() {

        setContentView(R.layout.map);

        //splash screen
        mLoadingView = findViewById(R.id.loadingView);
        mLoadingView.bringToFront(); //in case API version is less than 21 (translationZ in layout wouldn't work then)

        mRecyclerView = (RecyclerView) findViewById(R.id.waypointListView);
        colorArray = getResources().getIntArray(R.array.color_int_array);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mLatLongText = (TextView) findViewById(R.id.latLongText);

        //find buttons and set initial state
        btnAddNew = (ImageButton) findViewById(R.id.btnAddNew);
        btnHome1 = (ImageButton) findViewById(R.id.btnHome1);
        btnGoToList = (ImageButton) findViewById(R.id.btnGoToList);
        btnGoToMap = (ImageButton) findViewById(R.id.btnGoToMap);
        btnHome2 = (ImageButton) findViewById(R.id.btnHome2);

        btnAddNew.setOnClickListener(listenerAdd);
        btnGoToList.setOnClickListener(listenerGoToList);
        btnGoToMap.setOnClickListener(listenerGoToMap);
        btnHome1.setOnClickListener(listenerGoToHome1);
        btnHome2.setOnClickListener(listenerGoToHome2);

    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onMapLongClick(LatLng latLng) {
        addWaypointDialog(latLng);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Waypoint waypoint = mapManager.getWaypointByMarker(marker);
        mapManager.updateSelectedWaypoint(waypoint);

        return true; //Return 'TRUE' to prevent GoogleMap by default moves the map center to the marker.
    }


    @Override
    protected void onResume() {

        super.onResume();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);

        if (mapManager == null) {
            mapManager = new MapManager(this, mRecyclerView, drawerLayout);
        }

        mapState = new MapState(this);

        //TODO is this enough to keep app from crashing?
        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
            mapFragment.getMapAsync(this);
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        mapState = new MapState(this);
        mapState.saveMapState(mapManager.mMap);
        mapManager.wpListAdapter.saveListState();
        //if (mGoogleApiClient != null) {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);

        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }

        mSensorManager.unregisterListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        mSensorManager.unregisterListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));

    }

    @Override
    public void onBackPressed() {
        if (this.drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            this.drawerLayout.closeDrawer(Gravity.RIGHT);
        } else {
            super.onBackPressed();
        }
    }

/* -------------------------------------------------------
*
*  Google map functions - start
*
---------------------------------------------------------*/

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapManager.mMap = googleMap;
        mapManager.mMap.setOnMapLongClickListener(this);
        mapManager.mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){

            @Override
            public boolean onMyLocationButtonClick()
            {
                mapManager.setInitialZoom();
                return true;
            }
        });

        mapState = new MapState(this);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mapManager.mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mapManager.mMap.setMyLocationEnabled(true);
            mapManager.setInitialZoom();
        }
/*
        CameraPosition position = mapState.getSavedCameraPosition();

        if (position != null) {
            Toast.makeText(MainActivity.this, "position isnt null!", Toast.LENGTH_SHORT).show();

            CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
            mapManager.mMap.moveCamera(update);
            mapManager.mMap.setMapType(mapState.getSavedMapType());
        } else {
            mapManager.setInitialZoom();
        }*/
    }

    protected synchronized void buildGoogleApiClient() {
        //https://stackoverflow.com/questions/37102240/cannot-resolve-symbol-mgoogleapiclient-android-studio
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void setupAndRequestLocationPermissions() {
        mLocationRequest = new LocationRequest(); //make a new location request for this connection
        mLocationRequest.setInterval(500); //checks for location update every 1000 ms
        mLocationRequest.setFastestInterval(500); // Do be careful to also throttle setFastestInterval(long) if you perform heavy-weight work after receiving an update - such as using the network.
        //mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(2); //10 meters
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                    mLocationCallback,
                    null);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        setupAndRequestLocationPermissions();
        mapManager.mMap.setOnMarkerClickListener((GoogleMap.OnMarkerClickListener) this);
        mapIsConnected = true;
        for(int i = mapManager.viewModel.getWaypointCount() - 1; i >= 0; i--){
            Marker newMarker = mapManager.mMap.addMarker(mapManager.setMarkerOptions(mapManager.viewModel.getWaypointByIndex(i)));
            mapManager.addMarkerToHolder(mapManager.viewModel.getWaypointByIndex(i), newMarker);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        mapManager.updateLocation(mLastLocation);
        updateDistanceText(mLastLocation);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Permission was granted.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mapManager.mMap.setMyLocationEnabled(true);

                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }
    }

    public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Location permissions request");
                builder.setMessage("Location permission is needed to show an accurate map.");
                AlertDialog dialog = builder.create();
                dialog.show();

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }



    /* -------------------------------------------------------
    *
    *  Google map functions - end
    *
    ---------------------------------------------------------*/

  /* -------------------------------------------------------
   *
   *  Map orientation functions - start
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  Map orientation functions - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  Check mark listeners - start
   *
   ---------------------------------------------------------*/

    View.OnClickListener listenerGoToList = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (!drawerLayout.isDrawerVisible(Gravity.RIGHT)) {
                drawerLayout.openDrawer(Gravity.RIGHT);
            }
        }
    };

    View.OnClickListener listenerGoToHome2= new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (drawerLayout.isDrawerVisible(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
            if (!drawerLayout.isDrawerVisible(Gravity.LEFT)) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        }
    };

    View.OnClickListener listenerGoToHome1= new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (!drawerLayout.isDrawerVisible(Gravity.LEFT)) {
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        }
    };

    View.OnClickListener listenerGoToMap = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            if (drawerLayout.isDrawerVisible(Gravity.RIGHT)) {
                drawerLayout.closeDrawer(Gravity.RIGHT);
            }
            if (drawerLayout.isDrawerVisible(Gravity.LEFT)) {
                drawerLayout.closeDrawer(Gravity.LEFT);
            }
        }
    };

    View.OnClickListener listenerEdit = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

        }
    };

    View.OnClickListener listenerAdd = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //get current location
            //TODO why isn't current location exact?
            LatLng latlng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            //prompt user to add waypoint for current location
            addWaypointDialog(latlng);
        }
    };

    View.OnClickListener listenerDelete = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            deleteWaypointDialog(mapManager.getSelectedMarkerId());
            //viewModel.delete(selectedMarkerId);

        }
    };

    /* -------------------------------------------------------
   *
   *  Check mark listeners - end
   *
   ---------------------------------------------------------*/


    /* -------------------------------------------------------
   *
   *  User action functions - start
   *
   ---------------------------------------------------------*/


    private void deleteWaypointDialog(final String selectedMarkerId) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.default_dialog, null);

        alertDialogBuilder.setView(dialogView);

        //TextView dialogText = (TextView) findViewById(R.id.default_dialog_content);
        //dialogText.setText("Delete this waypoint permanently?", TextView.BufferType.EDITABLE);

        AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //mapManager.getSelectedMarker().remove();
                //mapManager.getMarkerById(mapManager.getSelectedMarkerId()).remove();
                mapManager.deleteWaypoint(mapManager.getSelectedMarkerId());
                //currentPath.remove();
                //currentPathBackground.remove();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(MainActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void addWaypointDialog(LatLng latLng) {
        //https://stackoverflow.com/a/5763969/5650506
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.waypoint_details_dialog, null);

        ArrayList<ColorData> colorDataList = new ArrayList<ColorData>();

        for (int j = 0; j < colorArray.length; j++) {
            colorDataList.add(new ColorData(j));
        }

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(dialogView);

        DecimalFormat df = new DecimalFormat("#.#####");
        alertDialogBuilder.setTitle(R.string.add_waypoint_here);
        EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);
        EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);

        String lat = df.format(latLng.latitude);
        String lng = df.format(latLng.longitude);
        etLong.setText(lng, TextView.BufferType.EDITABLE);
        etLat.setText(lat, TextView.BufferType.EDITABLE);
        final Spinner colorSpinner = (Spinner) dialogView.findViewById(R.id.color_spinner);

        ImageSpinnerAdapter spinnerAdapter = new ImageSpinnerAdapter(this, R.layout.image_spinner_row,
                R.id.spinnerImage, colorDataList);

        colorSpinner.setAdapter(spinnerAdapter);

        // set dialog message
        AlertDialog.Builder builder = alertDialogBuilder.setCancelable(true).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                EditText etTitle = (EditText) dialogView.findViewById(R.id.waypoint_detail_title_id);
                EditText etNotes = (EditText) dialogView.findViewById(R.id.waypoint_detail_notes_id);
                EditText etLat = (EditText) dialogView.findViewById(R.id.waypoint_detail_latitude_id);
                EditText etLong = (EditText) dialogView.findViewById(R.id.waypoint_detail_longitude_id);

                String wpDetailsTitle = etTitle.getText().toString();
                String wpDetailsNotes = etNotes.getText().toString();
                Double wpDetailsLat = Double.parseDouble(etLat.getText().toString());
                Double wpDetailsLong = Double.parseDouble(etLong.getText().toString());
                int wpColorInt = colorArray[(int) colorSpinner.getSelectedItemId()];

                try {
                    mapManager.sendWaypointToMarker(wpDetailsLat, wpDetailsLong, wpDetailsTitle, wpDetailsNotes, wpColorInt);
                } catch (Exception e) {
                    Toast.makeText(MainActivity.this, "Error " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(MainActivity.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    /* -------------------------------------------------------
   *
   *  User action functions - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  Sensor events - start
   *
   ---------------------------------------------------------*/

    @Override
    public void onSensorChanged(SensorEvent event) {
        double previousOrientation = mapManager.logSensorChanged(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        previousAccuracy = (double) accuracy;
        /*if (previousAccuracy != 3) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.waypoint_details_dialog, null);
            alertDialogBuilder.setView(dialogView);

            TextView dialogText = (TextView) dialogView.findViewById(R.id.default_dialog_content);
            dialogText.setText("Your compass accuracy is not high! Please open google maps and recalibrate to resolve.");

            AlertDialog.Builder builder = alertDialogBuilder.setCancelable(false).setPositiveButton("Save", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //finish();
                }
            });

        }*/
        /*SENSOR_STATUS_ACCURACY_HIGH = 3
        SENSOR_STATUS_ACCURACY_MEDIUM = 2
        SENSOR_STATUS_ACCURACY_LOW = 1
        SENSOR_STATUS_UNRELIABLE = 0*/
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    /* -------------------------------------------------------
   *
   *  Sensor events - end
   *
   ---------------------------------------------------------*/

    /* -------------------------------------------------------
   *
   *  button functions - start
   *
   ---------------------------------------------------------*/

    public void openInfo(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("App Info");
        builder.setMessage("App version 1.0.0 \n\n Last updated 2018 \n\n With issues or bugs email fillmein@test.com");
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /* -------------------------------------------------------
   *
   *  button functions - end
   *
   ---------------------------------------------------------*/

}
