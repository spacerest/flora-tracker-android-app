package com.example.spacerest.floratracker.Utilities;

import com.example.spacerest.floratracker.CustomClasses.WaypointManager;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by spacerest on 1/16/18.
 */

public class JsonToJava {

    public JsonToJava(String jsonStr) throws IOException {
        //url = new URL(jsonUrl);
        //reader = new InputStreamReader(jsonUrl.openStream());
        /*String jsonStr =
                "{ 'title': 'List of flora tracker waypointManager'," +
                        "'notes': 'These are the waypointManager used during testing this app'," +
                        "'errors': []," +
                        "'total': 0," +
                        "'limit': 20," +
                        "'points':" +
                        "[{" +
                        "'id': 1," +
                        "'title': 'test waypoint 1'," +
                        "'dateAdded': '01/28/18'," +
                        "'lng': -87.6508213," +
                        "'lat': 41.8378638," +
                        "'notes': 'test wp notes'" +
                        "},{" +
                        "'id': 2," +
                        "'title': 'test waypoint 2'," +
                        "'dateAdded': '01/28/18'," +
                        "'lng': -87.6508213," +
                        "'lat': 41.8378638," +
                        "'notes': 'test wp notes'" +
                        "},{" +
                        "'id': 3," +
                        "'title': 'test waypoint 3'," +
                        "'dateAdded': '01/28/18'," +
                        "'lng': -87.6508213," +
                        "'lat': 41.8378638," +
                        "'notes': 'test wp notes'" +
                        "}]" +
                        "}";*/
        //wps = new Gson().toJson(jsonStr);
        wps2 = new Gson().fromJson(jsonStr, WaypointManager.class);
    }

    InputStreamReader reader;
    //WaypointManager wps;
    WaypointManager wps2;

    public WaypointManager getDownloadedWaypoints() {
        return wps2;
    }

    public static void main(String[] args) throws Exception {

    }


}
