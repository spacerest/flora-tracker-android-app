package com.example.spacerest.floratracker.tests;

import android.widget.ListView;
import android.widget.Toast;

import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.activities.BrowseWaypointsActivity;
import com.example.spacerest.floratracker.classes.Waypoint;
import com.example.spacerest.floratracker.classes.WaypointListAdapter;
import com.example.spacerest.floratracker.classes.Waypoints;
import com.example.spacerest.floratracker.utils.JavaToJson;
import com.example.spacerest.floratracker.utils.JsonToJava;
import com.example.spacerest.floratracker.utils.CustomUtils;
import com.google.gson.Gson;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by SMP on 1/26/18.
 */

public class jsonTest {


    //GsonBuilder builder = new GsonBuilder();
    //Gson gson = builder.create();

    public jsonTest() throws IOException{
        //url = new URL("wft");//"http://77.81.234.178:8081/api/flora-tracker/flora-tracker");
        //reader = new InputStreamReader(url.openStream());
        //wps = new Gson().fromJson(reader, WaypointManager.class);
    }
    URL url;
    InputStreamReader reader;

    public static void main(String[] args) throws Exception {
        Waypoints wps = new Waypoints();

        try {
            String fileName = "waypointManager.json";

            String testJsonStr =
                    "{ 'title': 'List of flora tracker waypointManager'," +
                            "'notes': 'These are the waypointManager used during testing this app'," +
                            "'errors': []," +
                            "'total': 0," +
                            "'limit': 20," +
                            "'points':" +
                            "[{" +
                            "'id': 1," +
                            "'title': 'test waypoint 1'," +
                            "'dateAdded': '01/28/18'," +
                            "'lng': -87.6508213," +
                            "'lat': 41.8378638," +
                            "'notes': 'test wp notes'" +
                            "},{" +
                            "'id': 2," +
                            "'title': 'test waypoint 2'," +
                            "'dateAdded': '01/28/18'," +
                            "'lng': -87.6508213," +
                            "'lat': 41.8378638," +
                            "'notes': 'test wp notes'" +
                            "},{" +
                            "'id': 3," +
                            "'title': 'test waypoint 3'," +
                            "'dateAdded': '01/28/18'," +
                            "'lng': -87.6508213," +
                            "'lat': 41.8378638," +
                            "'notes': 'test wp notes'" +
                            "}]" +
                            "}";




            // END check and/or save new json file to internal storage

            // START read  json file from internal storage

            CustomUtils utils = new CustomUtils();



           // String jsonInputString = utils.convertStreamToString(is);
            //System.out.println("json input string is " + testJsonStr);

            //END read  json file from internal storage

            try {
                JsonToJava jsonToJava = new JsonToJava(testJsonStr);

                wps = jsonToJava.getDownloadedWaypoints();
                String sz = String.valueOf(wps.points.size());
                int wpCount = 0;

                //System.out.println(wps);

                //wps.set(0, wps.points.get(0));
            } catch (Exception e) {
                e.printStackTrace();
            }
            //END - get waypointManager from waypointManager file //

            JavaToJson javaToJson = new JavaToJson(wps);

            String gotString = javaToJson.getJsonString();

            System.out.println("got a string!" + gotString);

                    //Waypoint wpList[] = {wps.points.get(0), wps.points.get(1), wps.points.get(2)};

        } catch(Exception e) {

        }
    }
}
