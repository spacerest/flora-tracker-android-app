package com.example.spacerest.floratracker.UI;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import com.example.spacerest.floratracker.CustomClasses.Waypoint;
import com.example.spacerest.floratracker.R;
import com.google.android.gms.maps.model.Marker;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by SMP on 3/6/18.
 * With thanks to AbhiAndroid:
 * http://abhiandroid.com/ui/listview
 */

public class WaypointListAdapter extends RecyclerView.Adapter<WaypointListAdapter.ViewHolder> {
    Context context;
    ArrayList<Waypoint> waypointList;
    LayoutInflater inflater;
    ArrayList<Boolean> isCheckedArrayList;
    private SharedPreferences listStatePrefs;

    private static final String SELECTED_WAYPOINT_IDS = "ids";
    private static final String PREFS_NAME ="selectedIdsState";


    public WaypointListAdapter(Context applicationContext, ArrayList<Waypoint> waypointList, int size ) {
        this.context = applicationContext;
        this.waypointList = waypointList;
        inflater = (LayoutInflater.from(applicationContext));
        listStatePrefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        applySavedState();

        //this differs from example in tutorial:
        //has this.context = context and inflater = (LayoutInflater.from(applicationContext));
    }

    private void applySavedState() {
        isCheckedArrayList = new ArrayList<Boolean>(Collections.nCopies(waypointList.size(), false));
        HashSet<String> savedStateIds = (HashSet<String>) listStatePrefs.getStringSet(SELECTED_WAYPOINT_IDS, new HashSet<String>());


        for (Waypoint wp : waypointList) {
            if (savedStateIds.contains(wp.id)) {
                int position = getPositionById(wp.id);
                isCheckedArrayList.set(position, true);
            }
        }

    }

    public void saveListState() {
        SharedPreferences.Editor editor = listStatePrefs.edit();
        editor.putStringSet(SELECTED_WAYPOINT_IDS, getSelectedIds());
        editor.commit();
    }


    public Set<String> getSelectedIds() {

        int itemCount = getItemCount();
        Set<String> selectedIds = new HashSet<String>();;

        int checkCount = 0;

        for (int i = 0; i <= itemCount - 1; i++) { //itemCount - 1; i >= 0; i--) {
            String listItemId = getWaypointId(i);
            if (itemIsChecked(i)) {
                selectedIds.add(listItemId);
            }
        }
        return selectedIds;
    }

    public void checkWaypoint(String id) {

        //TODO
    }

    public String getWaypointId(int position) {
        return this.waypointList.get(position).id;
    }

    public boolean itemIsChecked(int position) {
        return isCheckedArrayList.get(position); //isCheckedList[position];
    }

    public void addCheckToHolder() {
        isCheckedArrayList.add(true);
    }

    public void removeCheckFromHolder(String id) {
        int position = getPositionById(id);
        isCheckedArrayList.remove(position);
    }

    public int getPositionById(String id) {
        int listLength = getItemCount();
        int position = -1;
        for (int i = 0; i < listLength; i++) {
            if (id == getWaypointId(i)) {
                position = i;
            }
        }
        return position;
    }

    @Override
    public long getItemId(int position) {
        return waypointList.get(position).intId;
    }

    @Override
    public int getItemCount() {
        return waypointList.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.bindData(waypointList.get(position));

        //in some cases, it will prevent unwanted situations
        holder.checkBox.setOnCheckedChangeListener(null);

        holder.checkBox.setChecked(isCheckedArrayList.get(position));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if((buttonView).isChecked()) {
                    isCheckedArrayList.set(position, true);
                }
                else {
                    isCheckedArrayList.set(position, false);
                }
                WaypointListAdapter.this.notifyDataSetChanged();
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView title,notes,latlng, dateAdded;
        CheckBox checkBox;

        public ViewHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.waypointTitleInList);
            notes = (TextView) itemView.findViewById(R.id.waypointDetailInList);
            latlng = (TextView) itemView.findViewById(R.id.waypointLatLongInList);
            dateAdded = (TextView) itemView.findViewById(R.id.waypointDateInList);
            checkBox = (CheckBox) itemView.findViewById(R.id.waypointCbox);
        }

        public void bindData(Waypoint waypoint) {
            String dateAddedStr = waypoint.dateAdded;
            title.setText(waypoint.title.toString());
            notes.setText(waypoint.notes.toString());
            dateAdded.setText(dateAddedStr);//waypointList.get(position).dateAdded.toString());
            latlng.setText(waypoint.lat.toString()+ ", " + waypoint.lng.toString());
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.waypoint_list_view, parent, false);
        return new ViewHolder(v);
    }

}

