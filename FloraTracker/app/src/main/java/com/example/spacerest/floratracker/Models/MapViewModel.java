package com.example.spacerest.floratracker.Models;


import android.content.Context;

import com.example.spacerest.floratracker.CustomClasses.Waypoint;
import com.example.spacerest.floratracker.CustomClasses.WaypointManager;
import com.example.spacerest.floratracker.R;
import com.example.spacerest.floratracker.Utilities.CustomUtils;

import java.util.ArrayList;


/**
 * Created by SMP on 5/16/18.
 */

public class MapViewModel {
    public WaypointManager waypointManager;// = new LiveData<WaypointManager>();
    CustomUtils utils;
    String fileName;
    Context context;
    //public ArrayList<Waypoint> points;

    public ArrayList<Waypoint> getWaypointList() {
        return waypointManager.points;
    }

    public String getLastAddedWaypointId() {
        return this.getWaypointIdByIndex(this.getWaypointCount() - 1);
    }

    public Waypoint getLastAddedWaypoint() {
        return waypointManager.points.get(this.getWaypointCount() - 1);
    }

    public Waypoint getWaypointByIndex(int index) {
        return waypointManager.points.get(index);
    }

    public void init(Context context) {
        this.context = context;
        utils = new CustomUtils();
        fileName = context.getResources().getString(R.string.json_file_name);
        waypointManager = utils.getCurrentWaypoints(fileName, context);
    }

    public void delete(String waypointId) {
        waypointManager.deleteWaypoint(waypointId);
        utils.writeUpdatedJson(fileName, waypointManager, context);
    }

    public void add(Double lat, Double lng, String title, String notes, int colorInt){
        waypointManager.addWaypoint(lat, lng, title, notes, colorInt);
        utils.writeUpdatedJson(fileName, waypointManager, context);
    }

    public Waypoint getWaypointById(String id) {
        for (Waypoint waypoint: waypointManager.points ) {
            if (waypoint.id.equals(id)) {
                return waypoint;
            }
        }
        return null;
    }


    public int getWaypointCount() {
        return waypointManager.points.size();
    }

    public String getWaypointIdByIndex(int index) {
        return waypointManager.points.get(index).id;
    }

    public void edit(String waypointId, Double lat, Double lng, String title, String notes, int colorInt) {
        waypointManager.updateWaypoint(waypointId, lat, lng, title, notes, colorInt);
    }

    public Waypoint getWaypoint(String waypointId) {
        return waypointManager.findWaypoint(waypointId);
    }

    public ArrayList<Waypoint> getWaypointsList() {
        return waypointManager.points;
    }



}
