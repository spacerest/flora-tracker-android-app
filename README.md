# Flora Tracker - Android

![N|Solid](https://is3-ssl.mzstatic.com/image/thumb/Purple127/v4/0c/93/a2/0c93a2b3-8f8b-59d4-dc84-122ce4f28132/source/60x60bb.jpg)

Android version of Flora Tracker, an application in use by [Plants of Concern](https://www.plantsofconcern.org/).

Flora Tracker is a GPS tool designed for the Citizen Scientists of Plants Of Concern, a conservation program headquartered at Chicago Botanic Garden

# Features

  - Drop and edit waypoints representing "plants of concern" on an interactive map 
  - Compass orientation leading to existing waypoints
